﻿(function(window) {
buraco1 = function() {
	this.initialize();
}
buraco1._SpriteSheet = new createjs.SpriteSheet({images: ["b1.png"], frames: [[0,0,52,54,0,0,0],[54,0,52,54,0,0,0],[108,0,52,54,0,0,0],[162,0,52,54,0,0,0],[0,56,52,54,0,0,0],[54,56,52,54,0,0,0],[108,56,52,54,0,0,0],[162,56,52,54,0,0,0],[0,112,52,54,0,0,0],[54,112,52,54,0,0,0]]});
var buraco1_p = buraco1.prototype = new createjs.Sprite();
buraco1_p.Sprite_initialize = buraco1_p.initialize;
buraco1_p.initialize = function() {
	this.Sprite_initialize(buraco1._SpriteSheet);
	this.paused = false;
}
window.buraco1 = buraco1;
}(window));

