﻿(function(window) {
motor2 = function() {
	this.initialize();
}
motor2._SpriteSheet = new createjs.SpriteSheet({images: ["m2.png"], frames: [[0,0,183,203,0,0,0],[185,0,183,203,0,0,0],[370,0,183,203,0,0,0],[0,205,183,203,0,0,0],[185,205,183,203,0,0,0],[370,205,183,203,0,0,0],[0,410,183,203,0,0,0],[185,410,183,203,0,0,0],[370,410,183,203,0,0,0],[0,615,183,203,0,0,0]]});
var motor2_p = motor2.prototype = new createjs.Sprite();
motor2_p.Sprite_initialize = motor2_p.initialize;
motor2_p.initialize = function() {
	this.Sprite_initialize(motor2._SpriteSheet);
	this.paused = false;
}
window.motor2 = motor2;
}(window));

