﻿(function(window) {
motor1 = function() {
	this.initialize();
}
motor1._SpriteSheet = new createjs.SpriteSheet({images: ["m1.png"], frames: [[0,0,138,199,0,0,0],[140,0,138,199,0,0,0],[280,0,138,199,0,0,0],[0,201,138,199,0,0,0],[140,201,138,199,0,0,0],[280,201,138,199,0,0,0],[0,402,138,199,0,0,0],[140,402,138,199,0,0,0],[280,402,138,199,0,0,0],[0,603,138,199,0,0,0]]});
var motor1_p = motor1.prototype = new createjs.Sprite();
motor1_p.Sprite_initialize = motor1_p.initialize;
motor1_p.initialize = function() {
	this.Sprite_initialize(motor1._SpriteSheet);
	this.paused = false;
}
window.motor1 = motor1;
}(window));

