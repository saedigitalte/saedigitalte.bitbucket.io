﻿(function(window) {
motor5 = function() {
	this.initialize();
}
motor5._SpriteSheet = new createjs.SpriteSheet({images: ["m5.png"], frames: [[0,0,646,487,0,0,0],[648,0,646,487,0,0,0],[1296,0,646,487,0,0,0],[0,489,646,487,0,0,0],[648,489,646,487,0,0,0],[1296,489,646,487,0,0,0],[0,978,646,487,0,0,0],[648,978,646,487,0,0,0],[1296,978,646,487,0,0,0],[0,1467,646,487,0,0,0]]});
var motor5_p = motor5.prototype = new createjs.Sprite();
motor5_p.Sprite_initialize = motor5_p.initialize;
motor5_p.initialize = function() {
	this.Sprite_initialize(motor5._SpriteSheet);
	this.paused = false;
}
window.motor5 = motor5;
}(window));

