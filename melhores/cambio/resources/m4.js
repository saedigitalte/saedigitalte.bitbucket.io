﻿(function(window) {
motor4 = function() {
	this.initialize();
}
motor4._SpriteSheet = new createjs.SpriteSheet({images: ["m4.png"], frames: [[0,0,219,256,0,0,0],[221,0,219,256,0,0,0],[442,0,219,256,0,0,0],[663,0,219,256,0,0,0],[0,258,219,256,0,0,0],[221,258,219,256,0,0,0],[442,258,219,256,0,0,0],[663,258,219,256,0,0,0],[0,516,219,256,0,0,0],[221,516,219,256,0,0,0]]});
var motor4_p = motor4.prototype = new createjs.Sprite();
motor4_p.Sprite_initialize = motor4_p.initialize;
motor4_p.initialize = function() {
	this.Sprite_initialize(motor4._SpriteSheet);
	this.paused = false;
}
window.motor4 = motor4;
}(window));

