﻿(function(window) {
motor3 = function() {
	this.initialize();
}
motor3._SpriteSheet = new createjs.SpriteSheet({images: ["m3.png"], frames: [[0,0,212,241,0,-58,0],[214,0,212,241,0,-58,0],[428,0,212,241,0,-58,0],[642,0,212,241,0,-58,0],[0,243,212,241,0,-58,0],[214,243,212,241,0,-58,0],[428,243,212,241,0,-58,0],[642,243,212,241,0,-58,0],[0,486,212,241,0,-58,0],[214,486,212,241,0,-58,0]]});
var motor3_p = motor3.prototype = new createjs.Sprite();
motor3_p.Sprite_initialize = motor3_p.initialize;
motor3_p.initialize = function() {
	this.Sprite_initialize(motor3._SpriteSheet);
	this.paused = false;
}
window.motor3 = motor3;
}(window));

