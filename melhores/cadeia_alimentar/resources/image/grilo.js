﻿(function(window) {
grilho_sp = function() {
	this.initialize();
}
grilho_sp._SpriteSheet = new createjs.SpriteSheet({images: ["grilo.png"], frames: [[2,2,95,258,0,9.450000000000003,145.8],[139,2,132,244,0,73.45,152.8],[313,2,167,222,0,90.45,138.8],[2,302,151,237,0,82.45,147.8],[195,302,132,245,0,73.45,152.8],[369,302,112,254,0,62.45,160.8],[2,598,101,264,0,58.45,166.8],[145,598,103,260,0,55.45,164.8],[290,598,112,254,0,62.45,160.8],[2,904,122,248,0,68.45,155.8],[166,904,132,246,0,73.45,153.8],[340,904,142,242,0,78.45,150.8],[2,1194,151,236,0,82.45,146.8],[195,1194,159,229,0,86.45,142.8],[2,1472,167,222,0,90.45,138.8],[211,1472,145,237,0,79.45,147.8],[2,1751,119,249,0,66.45,155.8],[163,1751,95,258,0,51.45,161.8],[300,1751,95,258,0,51.45,161.8]]});
var grilho_sp_p = grilho_sp.prototype = new createjs.Sprite();
grilho_sp_p.Sprite_initialize = grilho_sp_p.initialize;
grilho_sp_p.initialize = function() {
	this.Sprite_initialize(grilho_sp._SpriteSheet);
	this.paused = false;
}
window.grilho_sp = grilho_sp;
}(window));

