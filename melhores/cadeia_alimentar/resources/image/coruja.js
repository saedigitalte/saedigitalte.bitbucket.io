﻿(function(window) {
coruja = function() {
	this.initialize();
}
coruja._SpriteSheet = new createjs.SpriteSheet({images: ["coruja.png"], frames: [[2,2,240,237,0,128.35,211.3],[2,2,240,237,0,128.35,211.3],[247,2,247,162,0,135.35,136.3],[247,2,247,162,0,135.35,136.3],[2,244,240,253,0,128.35,136.3],[2,244,240,253,0,128.35,136.3],[247,244,240,265,0,128.35,136.3],[247,244,240,265,0,128.35,136.3]]});
var coruja_p = coruja.prototype = new createjs.Sprite();
coruja_p.Sprite_initialize = coruja_p.initialize;
coruja_p.initialize = function() {
	this.Sprite_initialize(coruja._SpriteSheet);
	this.paused = false;
}
window.coruja = coruja;
}(window));

