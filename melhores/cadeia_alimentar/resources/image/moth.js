﻿(function(window) {
moth = function() {
	this.initialize();
}
moth._SpriteSheet = new createjs.SpriteSheet({images: ["moth.png"], frames: [[2,2,76,70,0,39.599999999999994,66.3],[83,2,124,59,0,65.6,53.3],[2,77,104,62,0,60.599999999999994,37.3],[111,77,79,83,0,41.599999999999994,37.3],[2,165,113,61,0,59.599999999999994,45.3]]});
var moth_p = moth.prototype = new createjs.Sprite();
moth_p.Sprite_initialize = moth_p.initialize;
moth_p.initialize = function() {
	this.Sprite_initialize(moth._SpriteSheet);
	this.paused = false;
}
window.moth = moth;
}(window));

