﻿(function(window) {
mosquito = function() {
	this.initialize();
}
mosquito._SpriteSheet = new createjs.SpriteSheet({images: ["mosquito.png"], frames: [[2,2,63,72,0,9.6,-10.45],[2,79,70,51,0,9.6,-31.45],[2,135,63,58,0,9.6,-40.45]]});
var mosquito_p = mosquito.prototype = new createjs.Sprite();
mosquito_p.Sprite_initialize = mosquito_p.initialize;
mosquito_p.initialize = function() {
	this.Sprite_initialize(mosquito._SpriteSheet);
	this.paused = false;
}
window.mosquito = mosquito;
}(window));

