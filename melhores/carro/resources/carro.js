﻿(function(window) {
carro = function() {
	this.initialize();
}
carro._SpriteSheet = new createjs.SpriteSheet({images: ["carro.png"], frames: [[5,5,527,305,0,234,37],[537,5,625,307,0,297,37],[1167,5,470,308,0,245,37],[1642,5,384,310,0,177,37],[5,320,386,311,0,200,37],[396,320,395,313,0,214,37],[796,320,393,315,0,169,37],[1194,320,465,315,0,228,37],[5,640,527,315,0,274,37],[537,640,569,314,0,271,37],[1111,640,452,312,0,195,37],[1642,5,384,310,0,177,37],[1568,640,385,309,0,157,37],[5,960,430,308,0,187,37],[440,960,384,306,0,198,37],[829,960,454,305,0,220,37]]});
var carro_p = carro.prototype = new createjs.Sprite();
carro_p.Sprite_initialize = carro_p.initialize;
carro_p.initialize = function() {
	this.Sprite_initialize(carro._SpriteSheet);
	this.paused = false;
}
window.carro = carro;
}(window));

