﻿(function(window) {
mao = function() {
	this.initialize();
}
mao._SpriteSheet = new createjs.SpriteSheet({images: ["mao.png"], frames: [[0,0,324,302,0,156.25,144.2],[326,0,304,277,0,150.25,133.2],[632,0,285,257,0,145.25,141.2],[0,304,198,227,0,89.25,117.19999999999999],[200,304,216,227,0,109.25,115.19999999999999],[418,304,249,230,0,137.25,123.19999999999999],[669,304,263,252,0,148.25,142.2],[0,558,271,257,0,151.25,142.2],[273,558,288,274,0,159.25,149.2],[0,304,198,227,0,85.25,117.19999999999999]]});
var mao_p = mao.prototype = new createjs.Sprite();
mao_p.Sprite_initialize = mao_p.initialize;
mao_p.initialize = function() {
	this.Sprite_initialize(mao._SpriteSheet);
	this.paused = false;
}
window.mao = mao;
}(window));

