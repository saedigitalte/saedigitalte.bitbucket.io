! function(t) {
    "use strict";
    var e = t(window),
        a = t("html, body"),
        i = t("img"),
        o = t("div"),
        n = t(".main"),
        s = t(".grids"),
        r = t(".site-name"),
        d = t(".entries"),
        c = t("article.grid"),
        l = t(".site-content"),
        h = t("#portfolio"),
        p = t(".embed"),
        m = t(".pic"),
        f = t(".dropdown"),
        u = t(".modal"),
        g = t(".modal-body img"),
        v = t(".modal-footer"),
        b = t("#comments-post"),
        y = t(".zoom"),
        w = t(".full"),
        x = t("#share-buttons"),
        C = !1,
        k = 0;

    function A() {
        var a, i = t(".metro").width();
        (t("li.person, .preview li, body.page-author article.grid, body.archive article.grid").each(function() {
            var e = t(this);
            e.css("height", e.width())
        }), t(".nav-post, #map").each(function() {
            var e = t(this);
            e.css("height", e.width() / 2)
        }), n.css("margin-top", n.offset().left), e.width() > 1920 && n.css("margin-top", 152), h.each(function() {
            var e = t(this);
            e.css("height", e.width() / 1.5)
        }), e.width() > 1024) && t(".metro .box, .post-link .box").each(function() {
            var e = t(this);
            e.on({
                mouseover: function() {
                    e.find(".hidden_el").show(), a = e.find(".bottom-box").innerHeight() - 4, e.find(".bottom-box").css({
                        "margin-top": "-" + a + "px"
                    })
                },
                mouseout: function() {
                    e.find(".hidden_el").hide(), a = e.find(".bottom-box").innerHeight() - 4, e.find(".bottom-box").css({
                        "margin-top": "-" + a + "px"
                    })
                }
            })
        });

        function o() {
            t(".grids, article, .site-content, .sidebar").removeAttr("style"), c.addClass("reset-grid"), s.removeClass("clearfix"), 1 == C && s.masonry("destroy"), m.removeClass("border"), t(".pic .ctop, .pic .cbottom").remove(), t(".ads img, .single img").removeClass("grayscale")
        }
        e.width() > 768 ? (t("body.post-format-standard .entry-header, body.page .entry-header").each(function(e) {
            var a = t(this);
            a.css({
                height: a.width(),
                "max-height": t(".entry-content").innerHeight() - t(".tag-links").height()
            })
        }), d.each(function() {
            var e = t(this);
            e.css("height", e.width() / 4)
        }), t("li.title-section").each(function() {
            var e = t(this);
            e.css("height", e.width())
        }), s.css({
            height: i
        }), t(".reset-grid").removeAttr("style"), c.removeClass("reset-grid"), s.masonry({
            columnWidth: ".single",
            percentPosition: !0,
            isAnimated: !1,
            itemSelector: ".grid"
        }), C = !0, t(".grid-2 .box").each(function() {
            var e = t(this);
            e.on({
                mouseover: function() {
                    e.find(".hidden_el").show(), e.find(".bottom-box").css({
                        top: 0,
                        "margin-top": 0
                    })
                },
                mouseout: function() {
                    e.find(".hidden_el").hide();
                    var t = e.find(".bottom-box").innerHeight() - 4;
                    e.find(".bottom-box").css({
                        top: "100%",
                        "margin-top": "-" + t + "px"
                    })
                }
            })
        })) : e.width() > 480 && e.width() <= 768 ? (o(), t(".reset-grid").each(function() {
            var e = t(this);
            e.css("height", e.width())
        }), d.each(function() {
            var e = t(this);
            e.css("height", e.width() / 2)
        }), t(".ads").each(function() {
            var e = t(this);
            e.css("min-height", e.find("img").height() + 90)
        }), t("li.person:odd").after('<div class="clearfix"></div>')) : e.width() <= 480 && (o(), h.removeAttr("style"), h.find("li").removeClass("grid").removeClass("double"), t(".reset-grid, .post-link, .grid-2").each(function() {
            var e = t(this);
            e.css("height", e.width())
        }), t(".nav-post, .entries").each(function() {
            t(this).css("height", "auto")
        }), t('a[aria-label="Previous"]').html('<span><i class="fa fa-caret-left"></i></span>'), t('a[aria-label="Next"]').html('<span><i class="fa fa-caret-right"></i></span>')), t(".bottom-box").each(function() {
            var e = t(this),
                a = e.innerHeight() - 4;
            e.css({
                "margin-top": "-" + a + "px"
            })
        }), t("figcaption").each(function() {
            var e = t(this),
                a = e.innerHeight();
            e.css({
                "margin-top": "-" + a + "px"
            })
        }), t("body.home .grids").each(function() {
            var e = t(this);
            "" != e.find(".single:eq(0)").html() && e.find(".single:eq(0)").addClass("cat"), "" != e.find(".single:eq(1)").html() && e.find(".single:eq(1)").addClass("adv")
        })
    }
    r.each(function() {
        var e = t(this),
            a = e.text().split(" ");
        e.html(a[0] + " <span>" + a[1] + "</span>")
    }), A(), e.width() > 768 && (t('.box, div.grid-2, .preview a, a[data-toggle="modal"]').prepend('<div class="border"></div>'), t(".border, .pic.border").prepend('<div class="ctop"></div><div class="cbottom"></div>')), p.prepend('<div class="ctop"></div><div class="cbottom"></div>');
    var T = t("li.comment").length;

    function q() {
        t(".modal-body .pic").imagesLoaded(function() {
            t(".modal-body").stop().animate({
                opacity: 1
            }, {
                step: function(e, a) {
                    t(this).css("transform", "scale(" + e + ")")
                },
                duration: 500
            }, "easeInExpo")
        })
    }

    function L() {
        var e = t(".full"),
            a = e.attr("width"),
            i = e.attr("height");
        e.parent().addClass("draggable"), e.stop().animate({
                "max-width": a,
                "max-height": i
            }, 0),
            function() {
                interact(".draggable").draggable({
                    inertia: !0,
                    restrict: {
                        restriction: "parent",
                        endOnly: !0
                    },
                    onmove: function(t) {
                        var e = t.target,
                            a = (parseFloat(e.getAttribute("data-x")) || 0) + t.dx,
                            i = (parseFloat(e.getAttribute("data-y")) || 0) + t.dy;
                        e.style.webkitTransform = e.style.transform = "translate(" + a + "px, " + i + "px)", e.setAttribute("data-x", a), e.setAttribute("data-y", i)
                    }
                })
            }(), y.addClass("zoom-out")
    }

    function O() {
        g.removeAttr("style"), g.parent().removeClass("draggable"), g.parent().removeAttr("data-x"), g.parent().removeAttr("data-y"), g.parent().removeAttr("style"), y.removeClass("zoom-out")
    }
    b.find("span").text(T), t(".featured-img").each(function() {
        var a = t(this),
            i = a.find("img").attr("src");
        e.width() > 768 ? a.parent().css({
            "background-image": "url(" + i + ")"
        }) : a.css({
            "background-image": "url(" + i + ")"
        }), a.find("img").remove()
    }), t('article[data-type="catalog"]').each(function() {
        var e = t(this);
        "" == e.html() && (e.addClass("empty-cat"), e.html("<h6>CATEGORY<br>BLOCK</h6>"))
    }), t('article[data-type="ads"]').each(function() {
        var e = t(this);
        "" == e.html() && (e.addClass("empty-ads"), e.html("<h6>ADVERTISEMENT<br>BLOCK</h6>"))
    }), t(".type-post .fa-quote-right").parent().parent().addClass("type-quote"), e.width() > 1024 && t('div[data-type="background"]').each(function() {
        var a = t(this);
        e.scroll(function() {
            var t = "50% " + -e.scrollTop() / a.data("speed") / 2 + "px";
            a.css({
                "background-position": t
            })
        })
    }), e.width() > 1024 ? (t("#main-menu").on({
        mouseover: function() {
            t(".menu, .navbar-main").addClass("open")
        },
        mouseout: function() {
            t(".menu, .navbar-main").removeClass("open")
        }
    }), f.on("mousemove", function() {
        var e = t(this);
        e.children(".submenu").css({
            "padding-top": e.position().top
        })
    }), t(".navbar-main, .navbar-main .submenu").css("height", l.innerHeight())) : (f.append('<span class="o-sbm"></span>'), t("#main-menu .header-icon").on("click", function() {
        t(this).parent().toggleClass("open"), t(".menu, .navbar-main").toggleClass("open")
    }), t(".o-sbm").on("click", function() {
        var e = t(this);
        e.toggleClass("open"), e.parent().toggleClass("open"), e.parent().children(".submenu").toggleClass("open")
    })), b.on("click", function() {
        a.animate({
            scrollTop: t(".post-comments").offset().top
        }, 500)
    }), t("a.popup").each(function() {
        var e = t(this),
            a = e.children("img").attr("src"),
            i = e.next("figcaption").text();
        e.attr("data-url", a), e.attr("data-caption", i), e.attr("data-target", "#popup"), e.attr("href", "#popup"), l.before('<div class="modal" id="popup" tabindex="-1" role="dialog" aria-labelledby="popup" aria-hidden="true"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></button><div class="modal-dialog"><div class="modal-content"><div class="modal-body"></div></div></div></div>')
    }), t("#popup").on("show.bs.modal", function(a) {
        var i = t(a.relatedTarget),
            o = i.data("caption"),
            n = i.data("url"),
            s = t(this);
        e.width() > 768 ? s.find(".modal-body").html('<div class="pic border"><img src="' + n + '"><div class="modal-footer">' + o + '</div><div class="ctop"></div><div class="cbottom"></div></div>') : s.find(".modal-body").html('<div class="pic"><img src="' + n + '"><div class="modal-footer">' + o + "</div></div>"), q()
    }), t('a[data-toggle="modal"]').on("click", function() {
        t("header.site-header, .sidebar, .site-content, .site-footer").addClass("blur-on")
    });
    var E = 0;

    function H() {
        w.attr("width") < e.width() && w.attr("height") < e.height() ? y.hide() : y.show()
    }
    if (y.on("click", function() {
            u.toggleFullScreen(), 0 == E ? (L(), E = 1) : (O(), E = 0)
        }), t(document).keydown(function(t) {
            122 === t.keyCode && (0 == E ? (L(), E = 1) : (O(), E = 0)), 27 === t.keyCode && (O(), E = 0)
        }), t.fn.retina = function(e) {
            var a = {
                retina_part: "@2x"
            };
            return e && jQuery.extend(a, {
                retina_part: e
            }), window.devicePixelRatio >= 2 && this.each(function(e, i) {
                if (t(i).attr("src") && !new RegExp("(.+)(" + a.retina_part + "\\.\\w{3,4})").test(t(i).attr("src"))) {
                    var o = t(i).attr("src").replace(/(.+)(\.\w{3,4})$/, "$1" + a.retina_part + "$2");
                    t.ajax({
                        url: o,
                        type: "HEAD",
                        success: function() {
                            t(i).attr("src", o), t(i).each(function(t) {
                                $this.width($this.width() / 2)
                            })
                        }
                    })
                }
            }), this
        }, i.retina("@2x"), t(".preview a").on("click", function(t) {
            t.preventDefault()
        }), t("#gallery").on("show.bs.modal", function(e) {
            var a = t(e.relatedTarget);
            k = a.parent().index(), g.eq(k).addClass("full"), v.text(g.eq(k).attr("title")), q(), H(), t(".next").on("click", function() {
                k < g.length - 1 ? k++ : k = 0, g.removeClass("full"), g.parent().removeAttr("style"), g.parent().removeAttr("data-x"), g.parent().removeAttr("data-y"), g.eq(k).addClass("full"), 1 == E && L(), v.text(g.eq(k).attr("title")), H()
            }), t(".prev").on("click", function() {
                k > 0 ? k-- : k = g.length - 1, g.removeClass("full"), g.parent().removeAttr("style"), g.parent().removeAttr("data-x"), g.parent().removeAttr("data-y"), g.eq(k).addClass("full"), 1 == E && L(), v.text(g.eq(k).attr("title")), H()
            })
        }), t("#team-modal").on("show.bs.modal", function(e) {
            var a = t(e.relatedTarget),
                i = a.find(".person-desc").html(),
                o = a.next(".person-info").html(),
                n = a.find("img").attr("src"),
                s = t(this);
            TweenLite.set(t(".modal-dialog"), {
                transformOrigin: "50% 50% 0",
                scale: .5
            }), TweenLite.to(t(".modal-dialog"), .3, {
                opacity: 1,
                scale: 1,
                ease: Expo.easeInOut,
                delay: .3,
                onComplete: function() {
                    t(".modal-dialog").css({
                        "transform-origin": "",
                        transform: "",
                        "-webkit-transform": ""
                    })
                }
            }), s.find(".modal-body").html('<div class="person-box clearfix"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></button><div class="person-avatar"><img src="' + n + '"></div><div class="author-info">' + i + "</div>" + o + "</div>")
        }), t(".modal").on("hidden.bs.modal", function() {
            t(this).fullScreen(!1), g.removeClass("full"), t(".blur-on").removeClass("blur-on"), t("body, .modal-body").removeAttr("style"), O(), E = 0
        }), t(".tag-link").each(function() {
            var e = t(this),
                a = e.data("tag");
            e.css("opacity", a), e.hover(function() {
                e.stop().animate({
                    opacity: 1
                }, 250)
            }, function() {
                e.stop().animate({
                    opacity: a
                }, 250)
            })
        }), e.width() > 768 && o.hasClass("post")) {
        var z = t(".post-content p").eq(0).next().offset().top,
            $ = t(".post-content p").eq(0).next().position().top,
            _ = t(".post-content").innerHeight() - x.innerHeight() - $,
            I = t(".tag-links").offset().top - x.height();
        x.css({
            top: $ + "px"
        }), e.scroll(function() {
            var t = e.scrollTop(),
                a = 0;
            t > z && t < I ? a = t - z + 40 : t > I && (a = _), x.css({
                "margin-top": a + "px"
            })
        })
    }

    function W() {
        if (e.width() <= 480)
            for (var a = 0; a < t(".grid").length; a++) {
                var i = t(t(".grid")[a]),
                    o = i.find("img").attr("src");
                0 == i.find(".fullsizeWrap").length && (i.prepend('<div class="fullsizeWrap" style="left: 0px; top: 0px; width: 100%; height: 100%; overflow: hidden; position: absolute;"></div>'), i.find(".fullsizeWrap").css({
                    "background-image": "url(" + o + ")"
                }), i.find("img").css("display", "none"))
            } else t(".grid img").css("display", "block"), 0 == t(".fullsizeWrap").length && t(".grid img").liCover({
                parent: t(".grid"),
                position: "absolute"
            })
    }
    t("#btn-search").on("click", function() {
        TweenLite.to(t("#site-search"), .3, {
            transform: "rotateX(0)",
            opacity: 1
        }), TweenLite.to(t("#header"), .3, {
            css: {
                transform: "rotateX(-90deg)",
                transformOrigin: "0 0 7.5em",
                opacity: 0
            }
        }), t("#site-search").addClass("open")
    }), t("#site-search .close").on("click", function() {
        TweenLite.to(t("#site-search"), .3, {
            transform: "rotateX(90deg)",
            opacity: 0
        }), TweenLite.to(t("#header"), .3, {
            transform: "rotateX(0)",
            opacity: 1
        }), t("#site-search form")[0].reset(), t("#site-search").removeClass("open")
    }), Modernizr.input.placeholder || (t("form").find("*[placeholder]").each(function() {
        var e = t(this);
        e.val(e.attr("placeholder"))
    }).focusin(function() {
        $this.val("")
    }).focusout(function() {
        "" == $this.val() && $this.val($this.attr("placeholder"))
    }), t("form").submit(function() {
        var e = t(this);
        e.find("*[placeholder]").each(function() {
            var t = e.attr("placeholder");
            e.val() == t && e.val("")
        })
    })), e.on("load", function() {
        W(), t("#loader").fadeOut(0, function() {
            n.css("opacity", 1), e.width() > 480 && (TweenMax.staggerFromTo(t(".main-wrapper"), .3, {
                opacity: 0
            }, {
                css: {
                    opacity: 1
                },
                ease: Expo.easeInOut,
                delay: .3
            }), TweenLite.set(t(".main-wrapper"), {
                transformOrigin: "50% 10% 0",
                scale: .8
            }), TweenLite.to(t(".main-wrapper"), .3, {
                opacity: 1,
                scale: 1,
                ease: Expo.easeInOut,
                delay: .3,
                onComplete: function() {
                    t(".main-wrapper").css({
                        "transform-origin": "",
                        transform: "",
                        "-webkit-transform": ""
                    })
                }
            }))
        })
    }), e.resize(function() {
        A(), W()
    })
}(jQuery);