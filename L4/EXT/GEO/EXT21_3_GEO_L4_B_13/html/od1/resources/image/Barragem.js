const zzfxV = 0.3;
const zzfxX = new (window.AudioContext || webkitAudioContext);
const zzfx = (p = 1, k = .05, b = 220, e = 0, r = 0, t = .1, q = 0, D = 1, u = 0, y = 0, v = 0, z = 0, l = 0, E = 0, A = 0, F = 0, c = 0, w = 1, m = 0, B = 0) => { let M = Math, R = 44100, d = 2 * M.PI, G = u *= 500 * d / R / R, C = b *= (1 - k + 2 * k * M.random(k = [])) * d / R, g = 0, H = 0, a = 0, n = 1, I = 0, J = 0, f = 0, x, h; e = R * e + 9; m *= R; r *= R; t *= R; c *= R; y *= 500 * d / R ** 3; A *= d / R; v *= d / R; z *= R; l = R * l | 0; for (h = e + m + r + t + c | 0; a < h; k[a++] = f)++J % (100 * F | 0) || (f = q ? 1 < q ? 2 < q ? 3 < q ? M.sin((g % d) ** 3) : M.max(M.min(M.tan(g), 1), -1) : 1 - (2 * g / d % 2 + 2) % 2 : 1 - 4 * M.abs(M.round(g / d) - g / d) : M.sin(g), f = (l ? 1 - B + B * M.sin(d * a / l) : 1) * (0 < f ? 1 : -1) * M.abs(f) ** D * p * zzfxV * (a < e ? a / e : a < e + m ? 1 - (a - e) / m * (1 - w) : a < e + m + r ? w : a < h - c ? (h - a - c) / t * w : 0), f = c ? f / 2 + (c > a ? 0 : (a < h - c ? 1 : (h - a) / c) * k[a - c | 0] / 2) : f), x = (b += u += y) * M.cos(A * H++), g += x - x * E * (1 - 1E9 * (M.sin(a) + 1) % 2), n && ++n > z && (b += v, C += v, n = 0), !l || ++I % l || (b = C, u = G, n ||= 1); p = zzfxX.createBuffer(1, h, R); p.getChannelData(0).set(k); b = zzfxX.createBufferSource(); b.buffer = p; b.connect(zzfxX.destination); b.start(); return b };


const videoCheckpoints = [
  {
    start: 1.3,
  },
  {
    start: 6
  },
  {
    start: 9.5,
    choices: [
      ['Barragem à montante', 9.6, 'montante'],
      ['Barragem à jusante ', 11.5, 'jusante'],
      ['Barragem linha de centro ', 13.1, 'centro'],
      ['Rompimento de barragem', 14.6, 'rompimento']
    ]
  },
  {
    start: 10.6,
    backTo: 9.5,
  },
  {
    start: 11.3,
  },
  {
    start: 11.4,
    choices: [
      ['Voltar', 9.5]
    ]
  },

  {
    start: 12.1,
    backTo: 9.5,
  },
  {
    start: 12.7
  },
  {
    start: 12.8,
    choices: [
      ['Voltar', 9.5]
    ]
  },

  {
    start: 13.5,
    backTo: 9.5,
  },
  {
    start: 14.3
  },
  {
    start: 14.4,
    choices: [
      ['Voltar', 9.5]
    ]
  },

  {
    start: 15.0,
    backTo: 9.5,
  },
  {
    start: 16
  },
  {
    start: 34.7,
    choices: [
      ['Reiniciar', 0]
    ]
  },
]


class BarragemObjetoDigital extends ObjetoDigital {
  constructor(id) {
    super(id)
    this.name = 'Barragem'

    this._resources = {
      video: 'video.mp4',
      rewind: 'rewind.png',
      button: 'button.png',
      choices: 'choices.png',
      choicesInicio: 'choices_inicio.png',
      tutorialBackground: 'tutorial.png',
      phoneSpritesheet: 'phone_spritesheet.png',
      montanteChoices: 'choices_montante.png',
      jusanteChoices: 'choices_jusante.png',
      centroChoices: 'choices_centro.png',
      rompimentoChoices: 'choices_rompimento.png'
    }

    this.videoSkip = 0

    this.videoCheckThreashold = 0.1
    this.swipeThreashold = 190

    this.question = 0

    this.init()

    this._playing = false
    this.currentCheckPoint = null
    this.isChoice = false
    this.choiceReady = true
  }

  init() {
    if (window.DEV_MODE) {
      $(this.canvas).css('width', '1280px')
    }
    PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;
    this.app = new PIXI.Application({
      width: 1280,
      height: 720,
      resolution: window.devicePixelRatio,
      view: this.canvas
    })
    this.utils = new PixiUtils(PIXI, this.app)

    this.app.ticker.add(delta => this.tick(delta))

    for (const [k, v] of Object.entries(this._resources)) {
      this.app.loader.add(k, 'resources/image/' + v)
    }

    this.app.loader.load(async (_, resources) => {
      await ObjetoDigital.loadFont('Fira Sans', ObjetoDigital.geralRelativePath + '/fonts/fira/FiraSans-Bold.ttf')

      this.resources = resources
      this.postLoad()
    })

  }

  postLoad() {
    this.checkpoints = [...videoCheckpoints].map((v, i) => ({ ...v, index: i }))
    this.setupTutorial()

    this.ready()
  }

  initEvents() {
    let startPoint = null
    let finalPoint = null

    this.canvas.addEventListener('wheel', ev => {
      const { deltaY } = ev
      ev.preventDefault()
      if (!this.playing) {
        if (deltaY > 0) {
          this.next()
        } else {
          this.prev()
        }
      }
    })

    this.canvas.addEventListener('touchstart', ev => {
      // console.log(ev)
      ev.preventDefault()

      const [touch] = ev.touches
      if (touch) {
        startPoint = [touch.clientX, touch.clientY]
        // console.log(startPoint)
      }
    })

    this.canvas.addEventListener('touchmove', ev => {
      ev.preventDefault()

      const [touch] = ev.touches
      if (touch) {
        finalPoint = [touch.clientX, touch.clientY]
      }
    })

    this.canvas.addEventListener('touchend', ev => {
      ev.preventDefault()
      if (this.playing) return

      const [sourceX, sourceY] = startPoint
      const [targetX, targetY] = finalPoint

      const deltaX = targetX - sourceX
      const deltaY = targetY - sourceY
      const { swipeThreashold } = this

      console.log(Math.abs(deltaX), Math.abs(deltaY))
      if (swipeThreashold > Math.abs(deltaX) && swipeThreashold > Math.abs(deltaY)) return

      if (Math.abs(deltaX) > Math.abs(deltaY)) {
        if (deltaX < 0) {
          this.next()
        } else {
          this.prev()
        }
      } else {
        if (deltaY < 0) {
          this.next()
        } else {
          this.prev()
        }
      }
    })
  }

  createSpriteSheetAnimation(resource, w, h, frames) {
    const textures = []
    for (let i = 0; i < frames; i++) {
      textures.push(this.utils.loadTextureFromSpritesheet(resource, 0, h * i, w, h))
    }

    return new PIXI.AnimatedSprite(textures)
  }

  setupTutorial() {
    const scene = new PIXI.Container()
    this.app.stage.addChild(scene)

    scene.addChild(new PIXI.Sprite(this.resources.tutorialBackground.texture))

    const phone = this.createSpriteSheetAnimation(this.resources.phoneSpritesheet.texture, 430, 270, 4)
    phone.position.set(160, 20)
    phone.animationSpeed = .08
    phone.play()

    /*scene.addChild(phone)*/


    const btn1 = this.utils.loadTextureFromSpritesheet(this.resources.choicesInicio.texture, 0, 0, 187, 50)
    const btn2 = this.utils.loadTextureFromSpritesheet(this.resources.choicesInicio.texture, 0, 50, 187, 50)


    const btn = this.utils.createSimpleButton(
      [btn1, btn2],
      new PIXI.Text('', new PIXI.TextStyle({
        fill: 0x0,
        fontFamily: 'Fira Sans',
        fontSize: 35,
      })),
      () => {
        scene.visible = false
        this.initEvents()
        this.setupScene()
        this.startTimer()
        this.onStart()
      },
      1000 - btn1.width / 2, 610
    )


    scene.addChild(btn)
  }

  createSwipeEvents(gameScene) {

  }


  setupScene() {
    const gameScene = new PIXI.Container()
    this.app.stage.addChild(gameScene)

    const video = this.resources.video.data
    video.width = 1280
    video.height = 720
    this.video = video

    const texture = PIXI.Texture.from(this.video)
    const videoSprite = new PIXI.Sprite(texture)

    video.autoplay = false
    video.pause()
    video.volume = 0


    gameScene.addChild(videoSprite)

    const rewind = new PIXI.Sprite(this.resources.rewind.texture)
    rewind.tint = 0xff0000

    rewind.position.set(20, 20)
    rewind.scale.set(1.6, 1.6)
    rewind.visible = false

    this.rewindAnimation = anime({
      targets: rewind,
      alpha: [0, .7],
      direction: 'alternate',
      easing: 'linear',
      duration: 300,
      autoplay: false,
      loop: true
    })

    gameScene.addChild(rewind)

    const choices = new PIXI.Container()
    choices.name = 'Choices'

    const overlay = new PIXI.Graphics()
      .beginFill(0x0)
      .drawRect(0, 0, 1280, 720)
      .endFill()

    overlay.alpha = 0
    overlay.name = 'Overlay'

    choices.addChild(overlay)

    gameScene.addChild(choices)

    const btns = this.createButtons()

    gameScene.addChild(btns)

    this.createSwipeEvents(gameScene)


    this.items = {
      gameScene,
      videoSprite,
      rewind,
      btns,
      choices,
      overlay
    }
    // this.questionController.showDialog()
  }

  createButtons() {
    const btn1 = this.utils.loadTextureFromSpritesheet(this.resources.button.texture, 0, 0, 144, 144)
    const btn2 = this.utils.loadTextureFromSpritesheet(this.resources.button.texture, 144, 0, 144, 144)
    const btns = new PIXI.Container()
    btns.name = 'Buttons'

    this.buttons = {}


    const createButton = (isLeft) => {
      const btn = new PIXI.Sprite(btn1)

      btn.anchor.set(0.5, 0.5)
      if (isLeft) {
        btn.pivot.set(btn.width / 2, btn.height / 2 * -1)
        btn.position.set(0, 720)
        btn.rotation = Math.PI
      } else {
        btn.pivot.set(btn.width / 2, btn.height / 2)
        btn.position.set(1280, 720)
      }

      btn.interactive = true
      btn.buttonMode = true

      btn.on('pointerover', () => {
        btn.texture = btn2
      })

      btn.on('pointerout', () => {
        btn.texture = btn1
      })

      btn.on('pointerdown', () => {
        if (isLeft) this.prev()
        else this.next()
      })

      this.buttons[isLeft ? 'left' : 'right'] = btn

      return btn
    }

    const leftBtn = createButton(true)
    const rightBtn = createButton(false)

    btns.addChild(leftBtn)
    btns.addChild(rightBtn)



    btns.addChild(leftBtn)


    return btns
  }

  next() {
    if (!this.choiceReady) return
    if (this.playing || this.isChoice) return

    this.video.play()
    this.playing = true
  }

  prev() {
    if (!this.choiceReady) return
    if (this.playing) return

    if (this.isChoice) {
      this.isChoice = false
      this.playing = true
      this.animateChoiceOut(() => this._prev())
    } else {
      this._prev()
    }
  }

  _prev() {
    console.log('doing prev')


    let lastCheckpoint = this.checkpoints[this.currentCheckPoint.index - 1]
    if (!lastCheckpoint) return

    const backTo = this.currentCheckPoint.backTo

    if (backTo) {
      lastCheckpoint = this.checkpoints.find(c => c.start === backTo)
      console.log('backto', lastCheckpoint.start)
    }

    const lastCheckpointTime = lastCheckpoint.start - 0.08
    const currentIndex = this.currentCheckPoint.index



    lastCheckpoint.checked = false
    this.currentCheckPoint.checked = true
    this.playing = true
    this.items.rewind.visible = true
    this.rewindAnimation.play()
    this.video.playbackRate = 0.1
    this.video.play()

    const dur = this.video.currentTime - lastCheckpointTime

    const steps = 100
    const rewindDuration = 1200

    const step = dur / steps
    console.log(this.video.currentTime, lastCheckpointTime)
    console.log(dur, steps)

    const interval = setInterval(() => {
      this.video.currentTime -= step
      // console.log(this.video.currentTime)
    }, rewindDuration / steps)

    setTimeout(() => {
      clearInterval(interval)
      this.video.playbackRate = 1
      this.video.currentTime = lastCheckpointTime
      setTimeout(() => {
        this.rewindAnimation.pause()
        this.checkpoints[currentIndex].checked = false
        console.log(this.currentCheckPoint.index, this.currentCheckPoint.checked)
        this.items.rewind.visible = false

        this.checkTime()
      }, 200);
    }, rewindDuration);
  }

  handleChoice() {
    this.buttons.right.interactive = false

    const tl = anime.timeline()

    tl.add({
      targets: this.buttons.right,
      y: '+=256',
      easing: 'easeInCubic',
      duration: 600
    })

    tl.add({
      targets: this.items.overlay,
      alpha: .6,
      easing: 'linear',
      duration: 500,
    }, 400)

    const choices = this.currentCheckPoint.choices

    console.log(this.currentCheckPoint)

    const btn1 = this.utils.loadTextureFromSpritesheet(this.resources.choices.texture, 0, 0, 482, 76)
    const btn2 = this.utils.loadTextureFromSpritesheet(this.resources.choices.texture, 0, 76, 482, 76)

    const btnsGap = 20
    const btnsHeight = choices.length * (btn1.height + (choices.length > 1 ? btnsGap : 0)) - (choices.length > 1 ? btnsGap : 0)
    console.log(btnsHeight, btn1.height)

    for (let i = 0; i < choices.length; i++) {
      const choice = choices[i]

      let [b1, b2] = [btn1, btn2]

      const cid = choice[2]
      if (cid) {
        b1 = this.utils.loadTextureFromSpritesheet(this.resources[`${cid}Choices`].texture, 0, 0, 482, 76)
        b2 = this.utils.loadTextureFromSpritesheet(this.resources[`${cid}Choices`].texture, 0, 76, 482, 76)
      }

      const x = 1280 / 2
      const y = 720 / 2 - (btnsHeight / 2) + (i * (btn1.height + btnsGap)) + btn1.height / 2

      const btn = this.utils.createSimpleButton(
        [b1, b2],
        new PIXI.Text('   ' + choice[0], new PIXI.TextStyle({
          fill: 0x0,
          fontFamily: 'Fira Sans',
          fontSize: 28,
        })),
        () => {
          this.handleChoiceClick(choice)
        },
        x, y
      )

      btn.pivot.set(btn.width / 2, btn.height / 2)

      btn.scale.set(0, 0)
      btn.alpha = 0
      btn.isBtn = true

      this.items.choices.addChild(btn)

      tl.add({
        targets: btn,
        alpha: 1,
        easing: 'easeOutCubic',
        duration: 300,
      }, 800 + (i * 150))
      tl.add({
        targets: btn.scale,
        x: 1,
        y: 1,
        easing: 'easeOutCubic',
        duration: 300,
      }, 800 + (i * 150))

    }

    tl.add({
      targets: {},
      alpha: 1,
      begin: () => {
        this.choiceReady = true
        this.clicked = false
        this.items.btns.alpha = 1
      }
    })
  }

  handleChoiceClick([, position]) {
    if (!this.isChoice) return
    if (this.clicked) return
    this.clicked = true
    this.video.currentTime = position

    this.checkpoints
      .filter(c => c !== this.currentCheckPoint)
      .forEach(c => {c.checked = false})

    this.animateChoiceOut(() => {
      this.video.play()
      this.isChoice = false
      this.clicked = false
      this.playing = true
    })
  }

  animateChoiceOut(cb) {
    this.buttons.right.interactive = true
    const tl = anime.timeline()

    tl.add({
      targets: this.items.choices.children.filter(c => c.isBtn),
      alpha: 0,
      duration: 200,
      delay: anime.stagger(100),
      begin: () => console.log('begin choices alpha'),
      easing: 'easeInCubic',
      complete: () => {
        this.items.choices.children.filter(c => c.isBtn).forEach(c => c.destroy())
      }
    })

    tl.add({
      targets: this.items.choices.children.filter(c => c.isBtn).map(b => b.scale),
      x: 0,
      y: 0,
      duration: 200,
      delay: anime.stagger(100),
      easing: 'easeInCubic',
    }, 0)

    tl.add({
      targets: this.items.overlay,
      alpha: 0,
      duration: 300,
      easing: 'linear',
      begin: () => console.log('begin choices overlay'),
    }, 800)

    tl.add({
      targets: this.buttons.right,
      y: '-=256',
      easing: 'easeOutCubic',
      duration: 400,
      easing: 'linear',
    }, 800)

    tl.add({
      targets: {},
      alpha: 1,
      duration: 300,
      begin: () => cb()
    })
  }

  onStart() {
    if (this.video) this.video.play()
    this.playing = true
  }


  startTimer() {
    setInterval(() => {
      this.checkTime()
    }, 50);
  }

  checkTime() {
    const { currentTime } = this.video
    if (!this.playing) return

    for (const checkpoint of this.checkpoints) {
      const { start, end, checked, choices } = checkpoint
      if (!checked) {
        if (currentTime - this.videoCheckThreashold < start && currentTime + this.videoCheckThreashold > start) {
          console.log('CHECKPOIUNT ', checkpoint.index)
          this.playing = false
          checkpoint.checked = true
          this.video.pause()
          this.currentCheckPoint = checkpoint

          if (choices) {
            this.isChoice = true
            this.choiceReady = false
            this.items.btns.alpha = 0.35
            this.handleChoice()
          }
        }
        // } else if (!finished) {
        //   if (currentTime - this.videoCheckThreashold < end && currentTime + this.videoCheckThreashold > end) {
        //     this.playing = false
        //     checkpoint.finished = true
        //     this.video.pause()
        //   }
      }
    }
  }

  tick() {
  }

  get playing() {
    return this._playing
  }

  set playing(value) {
    this._playing = value
    this.items.btns.alpha = value ? 0.35 : 1
    this.items.btns.children.forEach(btn => {
      btn.cursor = value ? 'not-allowed' : 'pointer'
    })
  }

}