﻿(function(window) {
bat = function() {
	this.initialize();
}
bat._SpriteSheet = new createjs.SpriteSheet({images: ["bat.png"], frames: [[0,0,145,123,0,56.95,100.8],[147,0,207,96,0,92.95,67.8],[0,125,254,53,0,121.95,16.799999999999997],[256,125,194,95,0,97.95,26.799999999999997],[0,222,111,121,0,57.95,29.799999999999997],[113,222,105,130,0,54.95,32.8],[220,222,144,80,0,71.95,39.8],[0,354,198,62,0,91.95,52.8],[200,354,172,104,0,73.95,93.8]]});
var bat_p = bat.prototype = new createjs.Sprite();
bat_p.Sprite_initialize = bat_p.initialize;
bat_p.initialize = function() {
	this.Sprite_initialize(bat._SpriteSheet);
	this.paused = false;
}
window.bat = bat;
}(window));

