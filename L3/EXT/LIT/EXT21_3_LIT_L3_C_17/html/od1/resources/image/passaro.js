﻿(function(window) {
passaro = function() {
	this.initialize();
}
passaro._SpriteSheet = new createjs.SpriteSheet({images: ["passaro.png"], frames: [[0,0,204,210,0,79.85,145.05],[204,0,212,168,0,111.85,103.05000000000001],[416,0,245,122,0,126.85,57.05000000000001],[661,0,222,115,0,122.85,17.05000000000001],[0,210,170,178,0,89.85,17.05000000000001],[170,210,176,120,0,95.85,33.05000000000001],[346,210,206,150,0,103.85,85.05000000000001]]});
var passaro_p = passaro.prototype = new createjs.Sprite();
passaro_p.Sprite_initialize = passaro_p.initialize;
passaro_p.initialize = function() {
	this.Sprite_initialize(passaro._SpriteSheet);
	this.paused = false;
}
window.passaro = passaro;
}(window));

