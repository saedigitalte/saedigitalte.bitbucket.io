
const CardColor = {
  GREEN: 0,
  YELLOW: 1
}

const Constants = {
  CARD_SIDE_GAP: 12,
  UP_CARD_WF: 0.6,
  UP_CARD_HF: 0.6,
  PLACEHOLDERS_GAP: -480,
  QUESTION_TEXT_POS: [67, 50],
  QUESTION_TEXT_SCALE: [500, 170],
  ITEMS_RESULT_SPRITESHEET_SIZE: [100, 100],
  BUTTON_HEIGHT_ANIM: 70,
  PLACEHOLDER_POS_YELLOW: [430, 236],
  PLACEHOLDER_POS_GREEN: [836, 236],
  BALLON_POS_YELLOW: [-12, 379],
  BALLON_POS_GREEN: [757, 379]
}

const DialogType = {
  SUCCESS: 'Success',
  FAIL: 'Fail',
  FINISH: 'Finish'
}

class GeneticaObjetoDigital extends ObjetoDigital {
  constructor(id) {
    super(id)
    this.name = 'GeneticaMendeliana'
    this._scene = window.DEV_MODE ? 'game' : 'tutorial'
    this.scenes = {}

    this.state = {
      startParallax: 1,
      leftDeck: 1,
      rightDeck: 1,
      dragging: null,
      selectedCards: [null, null],
      actionAnimation: null,
      fails: 0,
      corrects: 0,
    }

    this._resources = {
      tutorial1: 'tutorial1.jpg',
      tutorial2: 'tutorial2.jpg',
      continuaBtn: 'continuarBtn.png',
      background: 'background.jpg',
      mendel: 'mendel.png',
      mendelHead: 'mendelHead.png',
      deck: 'deck.png',
      cardsSide: 'cardsSide.png',
      cardsUp: 'cardsUp.png',
      items1: 'items1.png',
      itemsResult: 'itemsResult.png',
      cardPlaceholder: 'cardPlaceholder.png',
      questionDeck: 'questionDeck.png',
      actionButton: 'actionButton.png',
      resultCards: 'resultCards.png',
      resultMessage: 'resultMessage.png',
      dialogSuccess: 'success.png',
      dialogFail: 'fail.png',
      dialogFinish: 'finish.png',
      playAgain: 'playAgain.png',
      audio: 'audio.png'
    }

    this.level = 0
    this.levels = [
      {
        cardsResource: 'items1',
        question: 'Qual deve ser o cruzamento entre a geração parental para que todos os indivíduos da geração F1 apresentem o genótipo e o fenótipo indicado?',
        card: {
          color: CardColor.YELLOW,
          name: 'VrRr',
          position: [0, 0],
          cardsResource: 'itemsResult'
        },
        cards: [
          {
            name: 'VVRR',
            position: [2, 1],
            color: CardColor.YELLOW
          },
          {
            name: 'Vvrr',
            position: [0, 0],
            color: CardColor.YELLOW,
            errorMessage: 'Para que todas as ervilhas sejam lisas, nenhum dos parentais pode ter fenótipo e genótipo rugoso.'
          },
          {
            name: 'vvRR',
            position: [1, 1],
            color: CardColor.GREEN,
            errorMessage: 'Para que todas as ervilhas sejam amarelas, nenhum dos parentais pode ter fenótipo e genótipo de cor verde.'
          },
          {
            name: 'vvrr',
            position: [0, 1],
            color: CardColor.GREEN
          },
          {
            name: 'vvrr',
            position: [2, 0],
            color: CardColor.YELLOW,
            errorMessage: 'A presença de dois alelos recessivos para a cor das ervilhas determina a cor verde, não amarela.'
          },
          {
            name: 'VVRR',
            position: [1, 0],
            color: CardColor.GREEN,
            errorMessage: 'A presença de dois alelos dominantes para a cor das ervilhas determina a cor amarela, não verde.'
          }
        ]
      },
      {
        cardsResource: 'itemsResult',
        question: 'Qual deve ser o cruzamento entre a geração parental para que metade dos indivíduos da geração F1 tenha ervilhas amarelas ou rugosas?',
        card: {
          color: CardColor.YELLOW,
          name: 'Vvrr',
          position: [1, 0],
          cardsResource: 'itemsResult'
        },
        cards: [
          {
            name: 'VvRr',
            position: [0, 0],
            color: CardColor.YELLOW
          },
          {
            name: 'VVrr',
            position: [1, 0],
            color: CardColor.YELLOW,
            errorMessage: 'Um parental com esse genótipo poderia originar apenas ervilhas de cor amarela, de forma que não haveria variação de cor.'
          },
          {
            name: 'vvRR',
            position: [2, 0],
            color: CardColor.GREEN,
            errorMessage: 'Para que metade das ervilhas seja amarela ou rugosa, cada parental precisa ter ao menos um alelo de cor amarela ou um alelo de ervilha rugosa.'
          },
          {
            name: 'vvrr',
            position: [3, 0],
            color: CardColor.GREEN
          },
          {
            name: 'vvRR',
            position: [1, 0],
            color: CardColor.YELLOW,
            errorMessage: 'A presença de dois alelos recessivos para a cor das ervilhas e de alelo dominante para a textura determina a ervilha de cor verde e lisa, não amarela e rugosa.'
          },
          {
            name: 'VVRR',
            position: [2, 0],
            color: CardColor.GREEN,
            errorMessage: 'A presença de dois alelos dominantes para a cor das ervilhas determina a cor amarela, não verde.'
          },
          {
            name: 'VVRR',
            position: [0, 0],
            color: CardColor.YELLOW,
            errorMessage: 'Para que metade das ervilhas seja amarela ou rugosa, cada parental precisa ter no máximo um alelo de cor amarela ou no mínimo um alelo de ervilha rugosa.'
          },
          {
            name: 'vvRr',
            position: [2, 0],
            color: CardColor.GREEN,
            errorMessage: 'Para atender ao que se pede, seriam necessários outro parental heterozigoto para a cor e outro homozigoto recessivo para a textura da semente.'
          }
        ]
      },
      {
        cardsResource: 'itemsResult',
        question: 'Qual deve ser o cruzamento entre a geração parental para que 6/16 dos indivíduos da geração F1 tenham ervilhas verdes e lisas?',
        card: {
          color: CardColor.GREEN,
          name: 'vvRr',
          position: [2, 0],
          cardsResource: 'itemsResult'
        },
        cards: [
          {
            name: 'VvRr',
            position: [0, 0],
            color: CardColor.YELLOW,
          },
          {
            name: 'VVrr',
            position: [1, 0],
            color: CardColor.YELLOW,
            errorMessage: 'Para que alguma das ervilhas da geração F1 seja verde ou lisa, cada parental precisa ter ao menos um alelo de cor verde ou um alelo de ervilha lisa.'
          },
          {
            name: 'vvRR',
            position: [2, 0],
            color: CardColor.GREEN,
            errorMessage: 'O genótipo desse parental origina apenas ervilhas lisas, não apresentando variação.'
          },
          {
            name: 'vvrr',
            position: [3, 0],
            color: CardColor.GREEN,
            errorMessage: 'Como suas duas características são recessivas, o outro parental precisaria ser duplo heterozigoto para ocorrer variação. Contudo, dentro dessas combinações, não seria possível obter 6/16 dos indivíduos da geração F1 com ervilhas verdes e lisas.'
          },
          {
            name: 'vvrr',
            position: [1, 0],
            color: CardColor.YELLOW,
            errorMessage: 'A presença de dois alelos recessivos para a cor das ervilhas determina a cor verde, não amarela.'
          },
          {
            name: 'VVrr',
            position: [2, 0],
            color: CardColor.GREEN,
            errorMessage: 'A presença de alelo dominante para a cor das ervilhas e dois recessivos para a textura determina uma ervilha de cor amarela e rugosa, não verde e lisa.'
          },
          {
            name: 'VVRR',
            position: [0, 0],
            color: CardColor.YELLOW,
            errorMessage: 'Para que alguma das ervilhas da geração F1 seja verde, cada parental precisa ter ao menos um alelo de cor verde.'
          },
          {
            name: 'vvRr',
            position: [2, 0],
            color: CardColor.GREEN
          },
          {
            name: 'VvRR',
            position: [0, 0],
            color: CardColor.YELLOW,
            errorMessage: 'O genótipo desse parental origina apenas ervilhas lisas, não apresentando variação.'
          }
        ]
      },
      {
        cardsResource: 'itemsResult',
        question: 'Qual deve ser o cruzamento entre a geração parental para que 50% dos indivíduos da geração F1 apresentem pelo menos um dos fenótipos e 25% apresentem o mesmo genótipo do exemplo indicado?',
        card: {
          color: CardColor.GREEN,
          name: 'vvrr',
          position: [3, 0],
          cardsResource: 'itemsResult'
        },
        cards: [
          {
            name: 'VvRr',
            position: [0, 0],
            color: CardColor.YELLOW,
            errorMessage: 'Para atender ao que se pede, seria necessário outro parental com o genótipo duplo recessivo para essas características.'
          },
          {
            name: 'VVrr',
            position: [1, 0],
            color: CardColor.YELLOW,
            errorMessage: 'O genótipo desse parental origina apenas ervilhas amarelas, não permitindo que ocorra variação.'
          },
          {
            name: 'vvRR',
            position: [2, 0],
            color: CardColor.GREEN,
            errorMessage: 'O genótipo desse parental origina apenas ervilhas lisas, não permitindo que ocorra variação.'
          },
          {
            name: 'Vvrr',
            position: [1, 0],
            color: CardColor.YELLOW
          },
          {
            name: 'vvrr',
            position: [1, 0],
            color: CardColor.YELLOW,
            errorMessage: 'A presença de dois alelos recessivos para a cor das ervilhas determina a cor verde, não amarela.'
          },
          {
            name: 'VVRR',
            position: [2, 0],
            color: CardColor.GREEN,
            errorMessage: 'A presença de dois alelos dominantes para a cor das ervilhas determina a cor amarela, não verde.'
          },
          {
            name: 'VVRR',
            position: [0, 0],
            color: CardColor.YELLOW,
            errorMessage: 'Para que alguma das ervilhas da geração F1 seja verde ou rugosa, cada parental precisa ter ao menos um alelo de cor verde ou de ervilha rugosa.'
          },
          {
            name: 'vvRr',
            position: [2, 0],
            color: CardColor.GREEN
          },
          {
            name: 'VvRR',
            position: [0, 0],
            color: CardColor.YELLOW,
            errorMessage: 'O genótipo desse parental origina apenas ervilhas lisas, não permitindo que ocorra variação.'
          }
        ]
      }
    ].map(l => ({ ...l, cards: this.shuffle(l.cards) }))
    this.init()
  }

  init() {
    if (window.DEV_MODE) {
      $(this.canvas).css('width', '1280px')
    }
    this.app = new PIXI.Application({
      width: 1280,
      height: 720,
      resolution: window.devicePixelRatio,
      view: this.canvas
    })

    this.bump = new Bump(PIXI)
    this.tink = new Tink(PIXI, this.app.view)
    this.utils = new PixiUtils(PIXI, this.app)

    this.app.ticker.add(delta => this.tick(delta))

    for (const [k, v] of Object.entries(this._resources)) {
      this.app.loader.add(k, 'resources/image/' + v)
    }

    this.app.loader.add('particles', 'resources/image/sprite-particulas1.json')

    this.audio = new Audio('resources/image/sound.mp3')
    this.app.loader.load(async (_, resources) => {
      await ObjetoDigital.loadFont('Helvetica', ObjetoDigital.geralRelativePath + '/fonts/fira/FiraSans-Bold.ttf')

      this.resources = resources
      this.loadParticlesClip()
      this.postLoad()
    })
  }

  postLoad() {
    this.setupHomeScene()
    this.setupGameScene()

    this.updateScenes()

    // DEV APENAS
    if (window.DEV_MODE) setTimeout(() => {
      this.startPlayer()
      this.startGame()
    }, 0)
  }

  loadParticlesClip() {
    let textures = ["Símbolo 30000", "Símbolo 30001", "Símbolo 30002", "Símbolo 30003", "Símbolo 30004", "Símbolo 30005", "Símbolo 30006", "Símbolo 30007", "Símbolo 30008", "Símbolo 30009", "Símbolo 30010", "Símbolo 30011", "Símbolo 30012", "Símbolo 30013", "Símbolo 30014", "Símbolo 30015"]

    textures = textures.map(t => PIXI.Texture.from(t))

    this.resources.particlesTextures = textures
  }

  updateScenes() {
    this.scenes.home.visible = this._scene === 'tutorial'
    this.scenes.game.visible = this._scene === 'game'
  }

  setupHomeScene() {
    const homeScene = new PIXI.Container()
    this.scenes.home = homeScene

    let tut = 0

    const tutorial1 = new PIXI.Sprite(this.resources.tutorial1.texture)
    tutorial1.anchor.x = 0.5
    tutorial1.anchor.y = 0.5

    tutorial1.x = this.app.renderer.width / 2
    tutorial1.y = this.app.renderer.height / 2

    homeScene.addChild(tutorial1)

    this.app.stage.addChild(homeScene)

    const btnTexture1 = this.loadTextureFromSpritesheet(this.resources.continuaBtn.texture,  0, 0, 313, 98)
    const btnTexture2 = this.loadTextureFromSpritesheet(this.resources.continuaBtn.texture, 314, 0, 313, 98)


    const btn = this.utils.createButton([btnTexture1, btnTexture2, btnTexture2], 1125, 660)
    btn.anchor.set(0.5, 0.5)
    btn.scale.set(0.75, 0.75)
    homeScene.addChild(btn)

    btn.filters = [new PIXI.filters.DropShadowFilter({
      blur: 4,
      distance: 2,
      rotation: -90
    })]

    btn.on('pointerdown', () => {
      tut++
      if (tut === 2 &&this.scene === 'tutorial') {
        this.scene = 'game'
        this.startGame()
        this.startPlayer()
      } else {
        tutorial1.texture = this.resources.tutorial2.texture
      }
    })
  }

  startPlayer () {
    if (this.audio) {
      this.audio.play()
      this.audio.volume = .3
      this.audio.loop = true
    }
  }

  setupGameScene() {
    const scene = new PIXI.Container()
    this.scenes.game = scene
    this.app.stage.addChild(scene)
    scene.visible = false

    const bg = new PIXI.Sprite(this.resources.background.texture)
    bg.name = 'Background'
    PixiUtils.setAnchorToMiddle(bg)
    bg.x = 1280 / 2
    bg.y = 720 / 2
    scene.addChild(bg)

    const mendel = new PIXI.Container()
    mendel.name = 'Mendel'
    mendel.y = (720 / 2) - 40
    mendel.x = 1280 / 2

    const mendelSprite = new PIXI.Sprite(this.resources.mendel.texture)
    mendelSprite.anchor.set(0.5, 0.5)
    mendel.addChild(mendelSprite)

    const mendelHeads = [...new Array(5)].map((_, i) => this.loadTextureFromSpritesheet(this.resources.mendelHead.texture, i * 263, 0, 263, 276))
    const mendelHead = new PIXI.Sprite(mendelHeads[2])
    mendelHead.anchor.set(0.5, 0.5)
    mendelHead.position.set(-12, -215)

    scene.interactive = true
    scene.on('pointermove', ev => this.handleHeadMove(ev, mendelHead, mendelHeads))

    mendel.addChild(mendelHead)

    mendel.scale.set(0.9, 0.9)
    scene.addChild(mendel)

    const curtain = new PIXI.Graphics()
    curtain.name = 'Curtain'
    curtain.beginFill(0x000000)
    curtain.drawRect(0, 0, 1280, 720)
    curtain.endFill()
    curtain.alpha = 0
    scene.addChild(curtain)

    const leftDeck = new PIXI.Sprite(this.resources.deck.texture)
    leftDeck.name = 'LeftDeck'
    leftDeck.anchor.y = 0.5
    leftDeck.x = -1000
    leftDeck.y = (720 / 2)
    scene.addChild(leftDeck)

    const rightDeck = new PIXI.Sprite(this.resources.deck.texture)
    rightDeck.name = 'RightDeck'
    rightDeck.anchor.y = 0.5
    rightDeck.x = 2000
    rightDeck.y = (720 / 2)
    rightDeck.rotation = Math.PI
    scene.addChild(rightDeck)

    const cardsContainer = new PIXI.Container()
    scene.addChild(cardsContainer)

    const placeHolders = new PIXI.Container()
    placeHolders.name = 'Placeholders'

    this.createPlaceholder(CardColor.YELLOW, ...Constants.PLACEHOLDER_POS_YELLOW, placeHolders)
    this.createPlaceholder(CardColor.GREEN, ...Constants.PLACEHOLDER_POS_GREEN, placeHolders)

    scene.addChild(placeHolders)

    const questionContainer = new PIXI.Container()
    questionContainer.name = 'QuestionContainer'
    questionContainer.pivot.set(0.5, 1)
    questionContainer.position.set(1280 / 2, 721)
    questionContainer.position.set(1280 / 2, 721)
    scene.addChild(questionContainer)


    const dragContainer = new PIXI.Container()
    dragContainer.name = 'DragContainer'
    dragContainer.pivot.set(0.5, 0.5)
    scene.addChild(dragContainer)

    const overlayContainer = new PIXI.Container()
    overlayContainer.name = 'OverlayContainer'
    scene.addChild(overlayContainer)

    const vol1 = this.loadTextureFromSpritesheet(this.resources.audio.texture, 0, 0, 100, 99)
    const vol2 = this.loadTextureFromSpritesheet(this.resources.audio.texture, 100, 0, 100, 99)

    const volumeBtn = new PIXI.Sprite(vol1)
    volumeBtn.position.set(50, 50)
    volumeBtn.anchor.set(0.5, 0.5)
    volumeBtn.scale.set(0.6, 0.6)
    volumeBtn.interactive = true
    volumeBtn.buttonMode = true

    let vol = true

    volumeBtn.on('pointerover', () => {
      volumeBtn.alpha = .7
    })

    volumeBtn.on('pointerout', () => {
      volumeBtn.alpha = 1
    })

    volumeBtn.on('pointerdown', () => {
      vol = !vol
      if (vol) {
        volumeBtn.texture = vol1
        anime({
          targets: this.audio,
          volume: .3,
          easing: 'linear',
          duration: 600
        })
      } else {
        volumeBtn.texture = vol2
        anime({
          targets: this.audio,
          volume: 0,
          easing: 'linear',
          duration: 600
        })
      }
    })

    scene.addChild(volumeBtn)

    this.gameSprites = {
      bg,
      mendel,
      leftDeck,
      rightDeck,
      cardsContainer,
      dragContainer,
      placeHolders,
      questionContainer,
      curtain,
      overlayContainer
    }

  }

  handleHeadMove({ data }, head, heads) {
    const { x, y } = data.global

    let ti, rot

    if (x < 268) {
      ti = 0
      rot = y / 1200 * -1
    } else if (x >= 268 && x < 520) {
      ti = 1
      rot = y / 1200 * -1
    } else if (x >= 520 && x < 800) {
      ti = 2
      rot = 0
    } else if (x >= 800 && x < 1040) {
      ti = 3
      rot = y / 1200
    } else if (x >= 1040) {
      ti = 4
      rot = y / 1200
    } else {
      ti = 2
      rot = 0
    }

    head.rotation = rot
    head.texture = heads[ti]
  }

  createPlaceholder(side, x, y, grp) {
    const placeHolder = new PIXI.Container()
    placeHolder.name = ['Green', 'Yellow'][side] + 'BackPlaceholder'
    placeHolder.side = side
    placeHolder.position.set(x, y + Constants.PLACEHOLDERS_GAP)

    const sprite = new PIXI.Sprite(this.resources.cardPlaceholder.texture)
    sprite.isCardPlaceholder = true
    sprite.width *= Constants.UP_CARD_WF
    sprite.height *= Constants.UP_CARD_HF
    placeHolder.addChild(sprite)

    PixiUtils.setAnchorToMiddle(sprite)
    PixiUtils.setPivotToMiddle(placeHolder)

    grp.addChild(placeHolder)
  }

  startGame(first = true) {
    this.createCardsForLevel()
    this.createQuestionDeck()
    let ended = false

    if (first) {
      this.gameSprites.mendel.width += 200
      this.gameSprites.mendel.height += 200

      this.gameSprites.bg.width += 180
      this.gameSprites.bg.height += 180
    }

    const tl = anime.timeline({
      targets: this.state,
      duration: 3000,
    })

    if (first) {
      tl.add({
        targets: this.gameSprites.mendel,
        width: '-=200',
        height: '-=200',
        duration: 2000,
        easing: 'easeOutCubic',
      })

      tl.add({
        targets: this.gameSprites.bg,
        width: '-=120',
        height: '-=120',
        duration: 2000,
        easing: 'easeOutCubic',
      }, 0)
    }

    tl.add({
      duration: 1,
      complete: () => {
        this.animateCardsEntering()
      }
    }, '700')

    tl.add({
      targets: this.gameSprites.leftDeck,
      x: 0,
      easing: 'easeOutCirc',
      duration: 600,
    }, '400')

    tl.add({
      targets: this.gameSprites.rightDeck,
      x: 1280,
      easing: 'easeOutCirc',
      duration: 600
    }, '700')

    tl.add({
      rightDeck: 0,
      easing: 'easeOutCirc',
      duration: 600
    }, '700')

    tl.add({
      targets: this.gameSprites.placeHolders.children,
      y: `-=${Constants.PLACEHOLDERS_GAP}`,
      delay: anime.stagger(250),
      easing: 'easeOutExpo',
      duration: 1000
    }, '900')

    tl.add({
      targets: this.gameSprites.questionContainer,
      y: `-=${this.gameSprites.questionContainer.getChildByName('Deck').height}`,
      easing: 'easeOutCirc',
      duration: 900
    }, '1800')


  }

  animateCardsEntering() {
    const animateSide = (color) => {
      const side = this.cardItems[color]
      const data = [...new Array(this.currentLevel.cards.filter(c => c.color === color).length)].map(() => ({
        val: 0
      }))

      anime({
        targets: data,
        val: 1,
        delay: anime.stagger(250),
        duration: 400,
        easing: 'easeOutCirc',
        update: () => {
          side.cards.forEach((card, i) => {
            if (color == CardColor.GREEN) {
              card.x = - data[i].val * card.width + Constants.CARD_SIDE_GAP
            } else {
              card.x = data[i].val * card.width - Constants.CARD_SIDE_GAP
            }
          })
        }
      })
    }

    animateSide(CardColor.YELLOW)
    setTimeout(() => animateSide(CardColor.GREEN), 600)
  }

  tick(dt) {
    this.tink.update()
  }

  createQuestionDeck() {
    const container = this.gameSprites.questionContainer

    // Button

    const btnTexture = this.loadTextureFromSpritesheet(
      this.resources.actionButton.texture,
      0, 0,
      304, 61
    )

    const hoverTexture = this.loadTextureFromSpritesheet(
      this.resources.actionButton.texture,
      0, 62,
      304, 61
    )

    const btnY = -224
    const btn = new PIXI.Sprite(btnTexture)
    btn.anchor.set(0.5, 1)
    btn.y = btnY + Constants.BUTTON_HEIGHT_ANIM
    btn.visible = false

    btn.buttonMode = true
    btn.interactive = true
    btn.originY = btnY

    btn.on('pointerover', () => {
      btn.texture = hoverTexture,
        anime({
          targets: btn,
          y: btnY + 2,
          easing: 'easeInOutCubic',
          duration: 100
        })
    })

    btn.on('pointerout', () => {
      btn.texture = btnTexture
      anime({
        targets: btn,
        y: btnY - 2,
        easing: 'easeInOutCubic',
        duration: 100
      })
    })

    btn.on('pointerdown', () => this.merge())

    container.addChild(btn)

    this.gameSprites.actionBtn = btn

    // Deck
    const deck = new PIXI.Container()
    deck.name = 'Deck'

    const bg = new PIXI.Sprite(this.resources.questionDeck.texture)
    bg.anchor.set(0.5, 1)
    deck.addChild(bg)

    bg.filters = [new PIXI.filters.DropShadowFilter({
      blur: 3,
      distance: 1,
      rotation: -90
    })]

    container.y += bg.height

    const graph = new PIXI.Graphics()
    graph.visible = false
    graph.beginFill(0xDE32AA)
    graph.drawRect(...Constants.QUESTION_TEXT_POS, ...Constants.QUESTION_TEXT_SCALE)
    graph.endFill()
    graph.position.set(-(bg.width / 2), -(bg.height))
    deck.addChild(graph)

    const style = new PIXI.TextStyle({
      wordWrap: true,
      wordWrapWidth: Constants.QUESTION_TEXT_SCALE[0],
      fontFamily: 'Helvetica'
    })

    const text = new PIXI.Text(`FASE ${this.level + 1}: ` + this.currentLevel.question, style)
    text.position.set(-(bg.width / 2), -(bg.height))
    text.position.x += Constants.QUESTION_TEXT_POS[0]
    text.position.y += Constants.QUESTION_TEXT_POS[1]

    deck.addChild(text)

    const itemSprite = new PIXI.Sprite(this.loadTextureFromSpritesheet(
      this.resources.itemsResult.texture,
      this.level * Constants.ITEMS_RESULT_SPRITESHEET_SIZE[0],
      0,
      Constants.ITEMS_RESULT_SPRITESHEET_SIZE[0],
      Constants.ITEMS_RESULT_SPRITESHEET_SIZE[1]
    ))

    PixiUtils.setAnchorToMiddle(itemSprite)
    itemSprite.position.set(-(bg.width / 2), -(bg.height))
    itemSprite.position.x += 605 + (809 - 605) / 2 + 2
    itemSprite.position.y += 105
    itemSprite.scale.set(1.2, 1.2)

    const cardText = new PIXI.Text(this.currentLevel.card.name, new PIXI.TextStyle({
      fontFamily: 'Helvetica',
      fontSize: 40
    }))
    cardText.anchor.set(0.5)
    cardText.position.set(-(bg.width / 2), -(bg.height))
    cardText.position.x += 605 + (809 - 605) / 2
    cardText.position.y += 190


    deck.addChild(cardText)
    deck.addChild(itemSprite)
    container.addChild(deck)

  }

  createCardsForLevel() {
    const deckHeight = 618
    const deckY = 58
    this.cardItems = {}

    const createCards = (color) => {
      const group = new PIXI.Container()
      group.name = ['Green', 'Yellow'][color] + 'Cards'
      const cards = this.levels[this.level].cards
        .filter(c => c.color === color)

      const cardsSize = cards.length

      this.cardItems[color] = {
        group,
        cards: []
      }

      cards.forEach((card, i) => {
        const item = this.createSideCard(card, this.levels[this.level], color)
        item.cursor = 'drag'
        group.addChild(item)
        item.interactive = true;
        item.cursor = 'grab';
        item.id = nanoid(16)
        item.card = card

        item.on('pointerdown', ev => this.onDragStart(ev, item, card))
        item.on('pointerup', ev => this.onDragEnd(ev, item, card))
        item.on('pointerupoutside', ev => this.onDragEnd(ev, item, card))
        item.on('pointermove', ev => this.onDragMove(ev, item, card))

        item.on('pointerup', () => {
          item.alpha = 1
        })

        let finalCardX = item.width - Constants.CARD_SIDE_GAP
        if (color === CardColor.GREEN) finalCardX *= -1

        item.on('pointerover', () => {
          if (this.state.dragging) return

          anime({
            targets: item,
            easing: 'easeInOutQuad',
            alpha: 1,
            x: finalCardX + (Constants.CARD_SIDE_GAP * (color === CardColor.GREEN ? -1 : 1)),
            duration: 170
          })
        })

        item.on('pointerout', () => {
          if (this.state.dragging) return

          anime({
            targets: item,
            easing: 'easeInOutQuad',
            x: finalCardX,
            alpha: 1,
            duration: 170
          })
        })


        const graph = new PIXI.Graphics()

        const gap = (deckHeight - (item.height * (cardsSize))) / (cardsSize + (cardsSize > 5 ? -1 : 1))

        item.y = deckY + (cardsSize < 5 ? gap : 0) + ((gap + item.height) * i)

        this.cardItems[color].cards.push(item)
      })

      return group
    }

    const leftSide = createCards(CardColor.YELLOW)
    const rightSide = createCards(CardColor.GREEN)
    leftSide.x = -(leftSide.width)

    rightSide.pivot.x = rightSide.width
    rightSide.x = 1280 + rightSide.width

    const graph = new PIXI.Graphics()



    this.gameSprites.cardsContainer.addChild(leftSide)
    this.gameSprites.cardsContainer.addChild(rightSide)
  }

  onDragStart(event, item) {
    item.dragData = event.data
    this.state.dragging = item

    this.state.selectedCards[item.card.color] = null
    this.checkButton()

    const cardPlaceholderLast = this.gameSprites.placeHolders.children.find(p => p.side === item.card.color)
      .children.find(c => c.name === 'CardPlaceholder')
    if (cardPlaceholderLast) {
      cardPlaceholderLast.destroy()
    }

    const card = this.createUpCard(item.card, this.currentLevel, item.card.color)
    card.position.set(-(card.width / 2), -(card.height / 2))

    const cardPlaceholder = this.createUpCard(item.card, this.currentLevel, item.card.color)
    cardPlaceholder.position.set(-(cardPlaceholder.width / 2), -(cardPlaceholder.height / 2))
    cardPlaceholder.name = 'CardPlaceholder'
    PixiUtils.setPivotToMiddle(cardPlaceholder)
    cardPlaceholder.alpha = 0
    this.gameSprites.placeHolders.children.find(p => p.side === item.card.color).addChild(cardPlaceholder)

    this.gameSprites.dragContainer.addChild(card)

    anime({
      targets: [
        ...this.cardItems[CardColor.GREEN].cards.filter(c => c.id !== item.id),
        ...this.cardItems[CardColor.YELLOW].cards.filter(c => c.id !== item.id)
      ],
      easing: 'easeInOutQuad',
      alpha: .3,
      duration: 120
    })


    this.onDragMove(event, item, item.card)
  }

  onDragEnd(event, item) {
    if (!this.state.dragging) return

    item.dragData = null
    this.state.dragging = null

    this.gameSprites.dragContainer.children[0].destroy()
    const cardPlaceholder = this.gameSprites.placeHolders.children.find(p => p.side === item.card.color)
      .children.find(c => c.name === 'CardPlaceholder')
    if (cardPlaceholder) {
      if (cardPlaceholder.isCorrect) {
        cardPlaceholder.alpha = 1
        this.state.selectedCards[item.card.color] = item
      } else {
        cardPlaceholder.destroy()
      }
    }

    let finalCardX = item.width - Constants.CARD_SIDE_GAP
    if (item.card.color === CardColor.GREEN) finalCardX *= -1

    anime({
      targets: item,
      easing: 'easeInOutQuad',
      x: finalCardX,
      alpha: 1,
      duration: 170
    })

    anime({
      targets: [
        ...this.cardItems[CardColor.GREEN].cards.filter(c => c.id !== item.id),
        ...this.cardItems[CardColor.YELLOW].cards.filter(c => c.id !== item.id)
      ],
      easing: 'easeInOutQuad',
      alpha: 1,
      duration: 120
    })

    this.checkButton()
  }

  checkButton() {
    const canDo = this.state.selectedCards.filter(a => !!a).length === 2
    const { actionBtn } = this.gameSprites

    if (this.actionAnimation) {
      this.actionAnimation.pause()
    }

    if (canDo) {
      actionBtn.visible = true
      this.actionAnimation = anime({
        targets: actionBtn,
        y: actionBtn.originY,
        easing: 'easeOutCubic',
        duration: 400,
        complete: () => {
          actionBtn.interactive = true
          actionBtn.buttonMode = true
        }
      })
    } else {
      actionBtn.interactive = false
      actionBtn.buttonMode = false
      this.actionAnimation = anime({
        targets: actionBtn,
        y: actionBtn.originY + Constants.BUTTON_HEIGHT_ANIM,
        easing: 'easeOutCubic',
        duration: 400,
        complete: () => {
          actionBtn.visible = false

        }
      })
    }
  }

  onDragMove(event, item) {
    const { dragContainer, placeHolders } = this.gameSprites

    if (this.state.dragging && this.state.dragging.id === item.id) {
      const newPos = item.dragData.getLocalPosition(dragContainer.parent)
      dragContainer.position.set(newPos.x, newPos.y)

      const cardPlaceholder = placeHolders.children.find(p => p.side === item.card.color)
        .children.find(c => c.name === 'CardPlaceholder')

      if (PixiUtils.abCollision(dragContainer, placeHolders.children.find(p => p.side === item.card.color))) {
        cardPlaceholder.alpha = .3
        cardPlaceholder.isCorrect = true
      } else {
        cardPlaceholder.alpha = 0
        cardPlaceholder.isCorrect = false
      }
    }
  }

  merge() {
    const canDo = this.state.selectedCards.filter(a => !!a).length === 2
    if (!canDo) return

    const { actionBtn } = this.gameSprites
    const removeInt = (i) => {
      i.interactive = false
      i.buttonMode = false
    }

    removeInt(actionBtn)

    for (const side of Object.values(this.cardItems)) {
      for (const item of side.cards) {
        removeInt(item)
      }
    }


    const greenCorrect = !this.state.selectedCards[0].card.errorMessage
    const yellowCorrect = !this.state.selectedCards[1].card.errorMessage

    const resultCards = {}

    const correct = greenCorrect && yellowCorrect

    if (correct) this.state.corrects++
    else this.state.fails++

    for (const item of this.gameSprites.placeHolders.children) {
      for (const subItem of item.children) {
        if (subItem.isCardPlaceholder) subItem.visible = false
      }

      if (!correct) {
        const thisCorrect = [greenCorrect, yellowCorrect][item.side]
        const resultCard = new PIXI.Sprite(this.loadTextureFromSpritesheet(
          this.resources.resultCards.texture,
          thisCorrect ? 0 : 300, 0,
          299, 440
        ))
        resultCard.isResultCard = true
        resultCard.anchor.set(0.5, 0.5)
        resultCard.width *= Constants.UP_CARD_WF
        resultCard.height *= Constants.UP_CARD_HF
        resultCard.visible = false
        // resultCard.position.set(resultCard.width / 2, resultCard.height / 2)

        item.addChild(resultCard)

        resultCards[item.side] = resultCard
      }
    }

    const btnTexture1 = this.loadTextureFromSpritesheet(this.resources.continuaBtn.texture, 0, 0, 313, 98)
    const btnTexture2 = this.loadTextureFromSpritesheet(this.resources.continuaBtn.texture, 314, 0, 313, 98)

    const btnDist = 300
    const btn = this.utils.createButton([btnTexture1, btnTexture2, btnTexture2], 1280 / 2, 650 + btnDist)
    btn.anchor.x = 0.5
    btn.anchor.y = 0.5
    btn.scale.set(.9, .9)
    btn.interactive = false

    btn.filters = [new PIXI.filters.DropShadowFilter({
      blur: 3,
      distance: 2,
      rotation: -90
    })]

    this.currBtn = btn

    btn.visible = !correct

    btn.on('pointerdown', () => this.nextLevel())

    const tl = anime.timeline()

    tl.add({
      targets: [this.gameSprites.leftDeck, this.gameSprites.cardsContainer.getChildByName('YellowCards')],
      x: '-=' + this.gameSprites.leftDeck.width,
      easing: 'easeInExpo',
      duration: 700
    })

    tl.add({
      targets: [this.gameSprites.rightDeck, this.gameSprites.cardsContainer.getChildByName('GreenCards')],
      x: '+=' + this.gameSprites.rightDeck.width,
      easing: 'easeInExpo',
      duration: 700
    }, 0)

    tl.add({
      targets: this.gameSprites.questionContainer,
      y: '+=' + this.gameSprites.questionContainer.height,
      easing: 'easeInExpo',
      duration: 700
    }, 0)

    tl.add({
      targets: [this.gameSprites.mendel, this.gameSprites.placeHolders],
      y: '+=60',
      easing: 'easeInOutExpo',
      duration: 800
    }, 200)

    tl.add({
      targets: this.gameSprites.bg,
      y: '+=20',
      easing: 'easeInOutExpo',
      duration: 800
    }, 200)

    tl.add({
      targets: this.gameSprites.curtain,
      alpha: 0.7,
      easing: 'linear',
      duration: 200,
    }, 500)

    if (!correct) {
      const gp = (side) => this.gameSprites.placeHolders.children.find(c => c.side === side)

      tl.add({
        targets: [gp(1), gp(0)].map(p => p.scale),
        x: 0,
        easing: 'easeInExpo',
        duration: 500,
        complete: (a) => {
          [gp(1), gp(0)].forEach(ph => {
            ph.getChildByName('CardPlaceholder').visible = false
            ph.getChildByName('CardPlaceholder').visible = false

            ph.children.filter(c => c.isResultCard).forEach(ph => ph.visible = true)
          })
        }
      }, 700)

      tl.add({
        targets: [gp(1), gp(0)].map(p => p.scale),
        x: 1,
        easing: 'easeOutExpo',
        duration: 500,
        complete: () => {
          setTimeout(() => {
            if (!greenCorrect) this.showCardMessage(
              this.state.selectedCards[CardColor.GREEN],
              CardColor.GREEN,
              gp(0).getChildByName('CardPlaceholder'),
              gp(0).children.find(c => c.isResultCard)
            )
            if (!yellowCorrect) this.showCardMessage(
              this.state.selectedCards[CardColor.YELLOW],
              CardColor.YELLOW,
              gp(1).getChildByName('CardPlaceholder'),
              gp(1).children.find(c => c.isResultCard)
            )

            this.gameSprites.overlayContainer.addChild(btn)
          }, 1900)
        }
      }, 700 + 500)
    } else {
      this.gameSprites.overlayContainer.addChild(btn)
      const clip = new PIXI.AnimatedSprite(this.resources.particlesTextures)
      this.gameSprites.overlayContainer.addChild(clip)

      clip.loop = false
      clip.anchor.set(0.5, 0.5)
      clip.position.set(1280 / 2, 283)
      clip.stop()
      clip.visible = false

      const finalCard = this.createUpCard(this.currentLevel.card, this.currentLevel.card, this.currentLevel.card.color)
      finalCard.position.set(549, 163)
      finalCard.visible = false
      this.gameSprites.overlayContainer.addChild(finalCard)

      tl.add({
        targets: this.gameSprites.placeHolders.children,
        x: 1280 / 2,
        y: this.gameSprites.placeHolders.children[0].y,
        easing: 'easeInExpo',
        duration: 1350
      }, 1000)

      tl.add({
        targets: {},
        duration: 0,
        complete: () => {
          clip.visible = true
          clip.gotoAndPlay(0)

        }
      }, 2340)

      tl.add({
        targets: {},
        duration: 0,
        complete: () => {
          finalCard.visible = true
        }
      }, 2350)
    }

    tl.add({
      targets: btn,
      y: `-=${btnDist + (correct ? 180 : 0)}`,
      easing: 'easeOutExpo',
      duration: 1200,
      complete: () => {
        if (correct) this.createDialog(correct ? DialogType.SUCCESS : DialogType.FAIL)
        else {
          btn.interactive = true
        }
      }
    }, correct ? 3900 : 6200)

  }

  showCardMessage({ card }, side, placeholderCard, resultCard) {


    const sprite = new PIXI.Sprite(this.resources.resultMessage.texture)
    const ballon = new PIXI.Container()
    sprite.scale.set(1.11, 0.93)
    ballon.position.set(...(side === CardColor.GREEN ? Constants.BALLON_POS_GREEN : Constants.BALLON_POS_YELLOW))

    // ballon.x += side === CardColor.GREEN ? -98 : -9
    // ballon.y += 140
    ballon.alpha = 0

    if (side === CardColor.GREEN) {
      sprite.anchor.x = 1
      sprite.scale.x *= -1
    }

    const text = new PIXI.Text(card.errorMessage, new PIXI.TextStyle({
      align: 'center',
      fontFamily: 'Helvetica',
      wordWrap: true,
      wordWrapWidth: 430
    }))

    text.anchor.set(0.5, 0.5)
    text.position.set(side === CardColor.GREEN ? 261 : 270, 190)


    ballon.addChild(sprite)
    ballon.addChild(text)


    anime({
      targets: ballon,
      alpha: 1,
      easing: 'easeInExpo',
      duration: 400,
    })

    const { width, height } = resultCard.getLocalBounds()

    placeholderCard.visible = true

    anime({
      targets: resultCard.scale,
      x: 0,
      y: 0,
      alpha: 0,
      easing: 'easeInQuart',
      duration: 500
    })



    const { overlayContainer } = this.gameSprites
    overlayContainer.addChild(ballon)

  }

  createDialog(dialogType) {
    if (dialogType === DialogType.FAIL && this.state.fails < 3) return

    const dialogContainer = new PIXI.Container()
    dialogContainer.name = 'FinalDialog'


    const dialog = new PIXI.Container()

    this.gameSprites.overlayContainer.addChild(dialogContainer)

    const bg = new PIXI.Sprite(this.resources['dialog' + dialogType].texture)
    dialog.addChild(bg)

    dialog.position.set(1280 / 2, 720 / 2)
    dialog.pivot.set(1280 / 2, 720 / 2)
    dialog.scale.set(0, 0)

    const style = new PIXI.TextStyle({
      fontFamily: 'Helvetica',
      fill: 0xffffff,
      fontSize: 34
    })

    const fbwh = (438 - 347) / 2
    const fbhh = (442 - 395) / 2

    const acertos = new PIXI.Text(`${this.state.corrects}/4`, style)
    PixiUtils.setAnchorToMiddle(acertos)
    acertos.position.set(347 + fbwh, 395 + fbhh)
    dialog.addChild(acertos)

    if (dialogType === DialogType.SUCCESS) {
      const triesLeft = new PIXI.Text(`${3 - this.state.fails}`, style)
      PixiUtils.setAnchorToMiddle(triesLeft)
      triesLeft.position.set(520 + fbwh, 395 + fbhh)
      dialog.addChild(triesLeft)

      const btnTexture1 = this.loadTextureFromSpritesheet(this.resources.continuaBtn.texture, 0, 0, 313, 98)
      const btnTexture2 = this.loadTextureFromSpritesheet(this.resources.continuaBtn.texture, 314, 0, 313, 98)

      const btn = this.utils.createButton([btnTexture1, btnTexture2, btnTexture2], 1280 / 2, 720 - 50)
      btn.anchor.x = 0.5
      btn.anchor.y = 0.5
      btn.scale.set(.8, .8)

      dialog.addChild(btn)

      btn.on('pointerdown', () => this.nextLevel(true))
    } else {
      const btnTexture1 = this.loadTextureFromSpritesheet(this.resources.playAgain.texture, 0, 0, 372, 86)
      const btnTexture2 = this.loadTextureFromSpritesheet(this.resources.playAgain.texture, 373, 0, 372, 86)

      const btn = this.utils.createButton([btnTexture1, btnTexture2, btnTexture2], 1280 / 2, 720 - 50)
      btn.anchor.x = 0.5
      btn.anchor.y = 0.5
      btn.scale.set(.8, .8)

      dialog.addChild(btn)

      btn.on('pointerdown', () => {
        this.level = 0
        this.state.corrects = 0
        this.state.fails = 0
        this.nextLevel(true, true)
      })
    }

    const curtain = new PIXI.Graphics()
      .beginFill(0x000000)
      .drawRect(0, 0, 1280, 720)
      .endFill()

    curtain.alpha = 0
    dialogContainer.addChild(curtain)
    dialogContainer.addChild(dialog)

    if (this.currBtn) this.currBtn.interactive = false

    anime({
      targets: curtain,
      alpha: .5,
      easing: 'easeOutExpo',
      duration: 900
    })

    anime({
      targets: dialog.scale,
      x: 1,
      y: 1,
      easing: 'easeOutExpo',
      duration: 900
    })

  }

  nextLevel(dialog = false, skip = false) {
    if (this.state.fails >= 3) {
      return this.createDialog(DialogType.FAIL)
    }

    if ((this.level + 1) === this.levels.length) {
      return this.createDialog(DialogType.FINISH)
    }
    const { mendel, bg, placeHolders, curtain, overlayContainer, questionContainer } = this.gameSprites
    anime({
      targets: [curtain, overlayContainer],
      alpha: 0,
      duration: 800,
      easing: 'linear',
      complete: () => {
        overlayContainer.removeChildren()
        overlayContainer.alpha = 1
      }
    })

    
    if (dialog) {
      placeHolders.alpha = 0
    }

    anime({
      targets: [mendel, placeHolders],
      y: '-=60',
      easing: 'easeInOutExpo',
      duration: 800
    })

    anime({
      targets: [bg],
      y: '-=20',
      easing: 'easeInOutExpo',
      duration: 800
    })

    anime({
      targets: this.gameSprites.placeHolders,
      alpha: 0,
      duration: 200,
      easing: 'linear',
      complete: () => {

        this.gameSprites.cardsContainer.removeChildren()

        placeHolders.alpha = 1
        placeHolders.removeChildren()
        this.createPlaceholder(CardColor.YELLOW, ...Constants.PLACEHOLDER_POS_YELLOW, placeHolders)
        this.createPlaceholder(CardColor.GREEN, ...Constants.PLACEHOLDER_POS_GREEN, placeHolders)

        questionContainer.removeChildren()
        questionContainer.position.set(1280 / 2, 721)
        questionContainer.position.set(1280 / 2, 721)

        if (!skip) this.level++
        this.startGame(false)
      }
    })

  }

  createSideCard(card, level, color) {
    const cardItem = new PIXI.Container()
    const cardSprite = new PIXI.Sprite(this.loadTextureFromSpritesheet(
      this.resources.cardsSide.texture,
      0,
      color === CardColor.GREEN ? 270 / 2 : 0,
      302,
      270 / 2 - 1
    ))

    const itemSprite = new PIXI.Sprite(this.loadTextureFromSpritesheet(
      this.resources[level.cardsResource].texture,
      100 * card.position[0],
      100 * card.position[1],
      100,
      100
    ))

    const upperText = new PIXI.Text(card.name, new PIXI.TextStyle({
      fontStyle: 'bold',
      fontSize: 40,
      fontFamily: 'Helvetica'
    }))

    const { textX, itemX } = [
      { // green
        textX: 197,
        itemX: 14
      },
      { // yellow
        textX: 95,
        itemX: 174
      }
    ][color]

    const textY = 15
    const textFinalY = 116
    const textHeight = textFinalY - textY

    upperText.anchor.set(0.5, 0.5)

    upperText.x = textX
    upperText.y = 62

    const itemWidth = 120
    itemSprite.anchor.set(0.5, 0.5)
    itemSprite.width = itemWidth
    itemSprite.height = itemWidth
    itemSprite.x = itemX + (126 - 14) / 2
    itemSprite.y = 64

    cardItem.addChild(cardSprite)
    cardItem.addChild(itemSprite)
    cardItem.addChild(upperText) // 44

    cardItem.width *= 0.7
    cardItem.height *= 0.65

    return cardItem
  }

  createUpCard(card, level, color) {
    const cardItem = new PIXI.Container()
    const cardSprite = new PIXI.Sprite(this.loadTextureFromSpritesheet(
      this.resources.cardsUp.texture,
      color === CardColor.GREEN ? 300 : 0,
      0,
      299,
      440
    ))

    const itemSprite = new PIXI.Sprite(this.loadTextureFromSpritesheet(
      this.resources[level.cardsResource].texture,
      100 * card.position[0],
      100 * card.position[1],
      100,
      100
    ))

    const text = new PIXI.Text(card.name, new PIXI.TextStyle({
      fontStyle: 'bold',
      fontSize: 60,
      fontFamily: 'Helvetica'
    }))

    const textX = 26
    const textY = 345

    const itemPos1 = [26, 24]
    const itemPos2 = [252, 260]

    const itemWidth = itemPos2[0] - itemPos1[0] * 0.9

    text.anchor.set(0.5, 0.5)

    text.x = itemPos1[0] + (itemWidth / 2)
    text.y = textY

    PixiUtils.setAnchorToMiddle(itemSprite)
    itemSprite.width = itemWidth
    itemSprite.height = itemWidth
    itemSprite.x = itemPos1[0] + (itemPos2[0] - itemPos1[0]) / 2
    itemSprite.y = itemPos1[1] + (itemPos2[1] - itemPos1[1]) / 2

    cardItem.addChild(cardSprite)
    cardItem.addChild(itemSprite)
    cardItem.addChild(text)

    cardItem.width *= Constants.UP_CARD_WF
    cardItem.height *= Constants.UP_CARD_HF

    return cardItem
  }

  loadTextureFromSpritesheet(texture, x, y, w, h) {
    const rect = new PIXI.Rectangle(x, y, w, h)
    return new PIXI.Texture(texture.baseTexture, rect)
  }

  shuffle(array) {
    let m = array.length, t, i;

    while (m) {
      i = Math.floor(Math.random() * m--)

      t = array[m]
      array[m] = array[i]
      array[i] = t
    }

    return array
  }

  set scene(sc) {
    this._scene = sc
    this.updateScenes()
  }

  get scene() {
    return this._scene
  }

  get currentLevel() {
    return this.levels[this.level]
  }
}