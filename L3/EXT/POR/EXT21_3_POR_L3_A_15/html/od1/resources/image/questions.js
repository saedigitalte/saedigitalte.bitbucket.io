const questions = [
  {
    text: 'De acordo com o Novo Acordo Ortográfico, qual frase está correta?',
    options: [
      {
        text: 'Eles <b>crêem</b> na vitória do time.',
        msg: 'Ops! Esta não é a opção correta. Os encontros “êe” não são mais acentuados.'
      }, {
        text: 'Eles <b>creem</b> na vitória do time. ',
        msg: 'Sim, esta é a opção correta!  Os encontros “êe” não são mais acentuados.',
        correct: true
      }
    ],
    times: [18.95, 24.42]
  },
  {
    text: 'De acordo com o Novo Acordo Ortográfico, qual frase está correta?',
    options: [
      {
        text: 'Ele sempre foi um <b>heroi</b> para mim.',
        msg: 'Sim, esta é a opção correta! Os ditongos “éi” e “ói” de palavras paroxítonas não são mais acentuados.',
        correct: true
      }, {
        text: 'Ele sempre foi um <b>herói</b> para mim.',
        msg: 'Ops! Esta não é a opção correta. Os ditongos “éi” e “ói” de palavras paroxítonas não são mais acentuados.',
      }
    ],
    times: [28.32, 29.5]
  },
  {
    text: 'De acordo com o Novo Acordo Ortográfico, qual frase está correta?',
    options: [
      {
        text: 'Ele não <b>para</b>.',
        msg: 'Sim, esta é a opção correta! Os acentos diferenciais deixaram de existir.',
        correct: true
      }, {
        text: 'Ele não <b>pára</b>.',
        msg: 'Ops! Esta não é a opção correta. Os acentos diferenciais deixaram de existir.',
      }
    ],
    times: [33.7, 34.5]
  },
  {
    text: 'De acordo com as regras de concordância, qual frase está correta?',
    options: [
      {
        text: 'A entrada de animais é <b>proibido</b>.',
        msg: 'Ops! Esta não é a opção correta. Quando o núcleo do sujeito vier precedido de artigo, o adjetivo deve concordar em gênero e número com o substantivo.',
      }, {
        text: 'A entrada de animais é <b>proibida</b>.',
        msg: 'Sim, esta é a opção correta! Quando o núcleo do sujeito vier precedido de artigo, o adjetivo deve concordar em gênero e número com o substantivo.',
        correct: true
      }
    ],
    times: [37.4, 38.6]
  },
  {
    text: 'De acordo com as regras de concordância, qual frase está correta?',
    options: [
      {
        text: 'Comoramos <b>bastantes</b> roupas.',
        msg: 'Sim, esta é a opção correta! Quando a palavra “bastante” modificar um substantivo, ela terá valor de adjetivo e deverá concordar em número com o substantivo a que se refere.',
        correct: true
      }, {
        text: 'Compramos <b>bastante</b> roupas.',
        msg: 'Ops! Esta não é a opção correta. Quando a palavra “bastante” modificar um substantivo, ela terá valor de adjetivo e deverá concordar em número com o substantivo a que se refere.',
      }
    ],
    times: [41.8, 43]
  },
  {
    text: 'De acordo com as regras de concordância, qual frase está correta?',
    options: [
      {
        text: 'Juliana é <b>meia</b> desconfiada.',
        msg: 'Ops! Esta não é a opção correta. Quando a palavra “meio” modificar o adjetivo, sua função é de advérbio e, portanto, é invariável.',
      }, {
        text: 'Juliana é <b>meio</b> desconfiada.',
        msg: 'Sim, esta é a opção correta! Quando a palavra “meio” modificar o adjetivo, sua função é de advérbio e, portanto, é invariável.',
        correct: true
      }
    ],
    times: [46.7, 47.8]
  },
  {
    text: 'De acordo com as regras de regência, qual frase está correta?',
    options: [
      {
        text: 'Os astronautas retornaram <b>à</b> Terra',
        msg: 'Sim, esta é a opção correta! Ao se referir ao planeta Terra, o uso da crase será recomendado.',
        correct: true
      }, {
        text: 'Os astronautas retornaram <b>a</b> Terra. ',
        msg: 'Ops! Esta não é a opção correta. Ao se referir ao planeta Terra, o uso da crase será recomendado.',
      }
    ],
    times: [51.2, 52.6]
  },
  {
    text: 'De acordo com as regras de regência, qual frase está correta?',
    options: [
      {
        text: 'Depois de muito tempo, eu voltei <b>a</b> casa esta semana.',
        msg: 'Sim, esta é a opção correta! Se a palavra “casa” aparecer sem nenhum qualificativo, o uso da crase não será recomendado.',
        correct: true
      }, {
        text: 'Depois de muito tempo, eu voltei <b>à</b> casa esta semana.',
        msg: 'Ops! Esta não é a opção correta. Se a palavra “casa” aparecer sem nenhum qualificativo, o uso da crase não será recomendado.',
      }
    ],
    times: [56.8, 57.7]
  }
]
