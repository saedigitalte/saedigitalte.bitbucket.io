const zzfxV = 0.3;
const zzfxX = new (window.AudioContext || webkitAudioContext);
const zzfx = (p = 1, k = .05, b = 220, e = 0, r = 0, t = .1, q = 0, D = 1, u = 0, y = 0, v = 0, z = 0, l = 0, E = 0, A = 0, F = 0, c = 0, w = 1, m = 0, B = 0) => { let M = Math, R = 44100, d = 2 * M.PI, G = u *= 500 * d / R / R, C = b *= (1 - k + 2 * k * M.random(k = [])) * d / R, g = 0, H = 0, a = 0, n = 1, I = 0, J = 0, f = 0, x, h; e = R * e + 9; m *= R; r *= R; t *= R; c *= R; y *= 500 * d / R ** 3; A *= d / R; v *= d / R; z *= R; l = R * l | 0; for (h = e + m + r + t + c | 0; a < h; k[a++] = f)++J % (100 * F | 0) || (f = q ? 1 < q ? 2 < q ? 3 < q ? M.sin((g % d) ** 3) : M.max(M.min(M.tan(g), 1), -1) : 1 - (2 * g / d % 2 + 2) % 2 : 1 - 4 * M.abs(M.round(g / d) - g / d) : M.sin(g), f = (l ? 1 - B + B * M.sin(d * a / l) : 1) * (0 < f ? 1 : -1) * M.abs(f) ** D * p * zzfxV * (a < e ? a / e : a < e + m ? 1 - (a - e) / m * (1 - w) : a < e + m + r ? w : a < h - c ? (h - a - c) / t * w : 0), f = c ? f / 2 + (c > a ? 0 : (a < h - c ? 1 : (h - a) / c) * k[a - c | 0] / 2) : f), x = (b += u += y) * M.cos(A * H++), g += x - x * E * (1 - 1E9 * (M.sin(a) + 1) % 2), n && ++n > z && (b += v, C += v, n = 0), !l || ++I % l || (b = C, u = G, n ||= 1); p = zzfxX.createBuffer(1, h, R); p.getChannelData(0).set(k); b = zzfxX.createBufferSource(); b.buffer = p; b.connect(zzfxX.destination); b.start(); return b };



class QuizGramaticalObjetoDigital extends ObjetoDigital {
  constructor(id) {
    super(id)
    this.name = 'QuizGramatical'

    this._resources = {
      video: 'game.mp4',
      thumb: 'thumb.jpg',
      popup: 'popup.png',
      popupMask: 'popupMask.png',
      dialog1: 'dialog1.png',
      character: 'character.png',
      buttons: 'buttons.png',
      buttonsSmall: 'buttonsSmall.png',
      coin: 'coin.png',
      audio: 'audio.png'
    }

    this.videoSkip = 0

    this.constants = {
      speechWordDelay: 150,
      dialogContentWidth: 718 - 14,
      dialogContentLeftMargin: 14,
      dialogContentTopMargin: 111,
      dialogContentMargin: 24
    }

    this.question = 0

    this.level = {
      helpText: 'Olá! Eu preciso chegar até o fim destas plataformas para conseguir um troféu, e, para isso, preciso responder algumas perguntas de gramática. \n\nConsegue me ajudar?',
      endText: 'E aí? Como foi o seu desempenho? \n\nPara dominar a Língua Portuguesa é preciso esforço e dedicação, pois se trata de uma língua complexa. Mas, todo esse esforço vale muito a pena! Continue os seus estudos!'
    }

    this._coins = 0

    this.init()
  }

  init() {
    if (window.DEV_MODE) {
      $(this.canvas).css('width', '1280px')
    }
    PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;
    this.app = new PIXI.Application({
      width: 1280,
      height: 720,
      resolution: window.devicePixelRatio,
      view: this.canvas
    })
    this.utils = new PixiUtils(PIXI, this.app)

    this.app.ticker.add(delta => this.tick(delta))

    for (const [k, v] of Object.entries(this._resources)) {
      this.app.loader.add(k, 'resources/image/' + v)
    }

    this.audio = new Audio('resources/image/music.mp3')

    this.app.loader.add('characterAnimation', 'resources/image/characterSprite.json')
    this.app.loader.load(async (_, resources) => {
      await ObjetoDigital.loadFont('Helvetica', ObjetoDigital.geralRelativePath + '/fonts/fira/FiraSans-Bold.ttf')

      this.resources = resources
      this.postLoad()
    })
  }

  postLoad() {
    this.loadCharacter()
    this.setupScene()
    this.setVideoStops()
  }

  startPlayer () {
    if (this.audio) {
      this.audio.play()
      this.audio.volume = .1
      this.audio.loop = true
    }
  }

  loadCharacter() {
    let textures = ["frame0000.png", "frame0001.png", "frame0002.png", "frame0003.png", "frame0004.png", "frame0005.png", "frame0006.png", "frame0007.png", "frame0008.png", "frame0009.png"]

    textures = textures.map(t => PIXI.Texture.from(t))

    this.resources.characterTextures = textures
  }

  setupScene() {
    const gameScene = new PIXI.Container()
    this.app.stage.addChild(gameScene)

    const video = this.resources.video.data
    video.width = 1280
    video.height = 720
    this.video = video

    video.autoplay = false
    const texture = PIXI.Texture.from(this.video)
    video.autoplay = false
    const videoSprite = new PIXI.Sprite(texture)
    video.autoplay = false
    video.pause()

    texture.baseTexture.on('loaded', () => texture.baseTexture.source.pause());

    gameScene.addChild(videoSprite)

    const coinsContainer = new PIXI.Container()

    const coin = new PIXI.Sprite(this.resources.coin.texture)

    coinsContainer.addChild(coin)

    const coinText = new PIXI.Text('0', new PIXI.TextStyle({
      fill: 0xffffff,
      fontFamily: 'Helvetica',
      fontSize: 60,
      stroke: "#b75513",
      strokeThickness: 12,
    }))

    coinText.position.set(-(coin.width) - 5, 0)
    coinsContainer.addChild(coinText)

    gameScene.addChild(coinsContainer)

    coinsContainer.pivot.set(0.5, coinsContainer.height / 2)
    coinsContainer.position.set(1280 - 10 - (coinsContainer.width / 2), 10 + (coinsContainer.height / 2))


    const thumbnail = new PIXI.Sprite(this.resources.thumb.texture)
    thumbnail.width = 1280
    thumbnail.height = 720
    gameScene.addChild(thumbnail)

    thumbnail.buttonMode = true
    thumbnail.interactive = true

    thumbnail.on('pointerdown', () => {
      this.clickSound()
      this.start()
    })

    const curtain = new PIXI.Graphics()
    curtain.beginFill(0x000000)
    curtain.drawRect(0, 0, 1280, 720)
    curtain.endFill()
    curtain.alpha = 0
    gameScene.addChild(curtain)

    const initialDialog = this.createInitialDialog()
    gameScene.addChild(initialDialog)

    const questionDialog = this.createQuestionDialog()
    gameScene.addChild(questionDialog)

    questionDialog.position.set(
      1280 / 2 - questionDialog.width / 2,
      720 / 2 - questionDialog.height / 2
    )
    questionDialog.y += 800
    questionDialog.updateMask()

    const vol1 = this.utils.loadTextureFromSpritesheet(this.resources.audio.texture, 0, 0, 100, 99)
    const vol2 = this.utils.loadTextureFromSpritesheet(this.resources.audio.texture, 100, 0, 100, 99)

    const volumeBtn = new PIXI.Sprite(vol1)
    volumeBtn.position.set(50, 50)
    volumeBtn.anchor.set(0.5, 0.5)
    volumeBtn.scale.set(0.6, 0.6)
    volumeBtn.interactive = true
    volumeBtn.buttonMode = true

    let vol = true

    volumeBtn.on('pointerover', () => {
      volumeBtn.alpha = .7
    })

    volumeBtn.on('pointerout', () => {
      volumeBtn.alpha = 1
    })

    volumeBtn.on('pointerdown', () => {
      vol = !vol
      if (vol) {
        volumeBtn.texture = vol1
        anime({
          targets: this.audio,
          volume: .1,
          easing: 'linear',
          duration: 600
        })
      } else {
        volumeBtn.texture = vol2
        anime({
          targets: this.audio,
          volume: 0,
          easing: 'linear',
          duration: 600
        })
      }
    })

    gameScene.addChild(volumeBtn)


    this.items = {
      gameScene,
      videoSprite,
      thumbnail,
      curtain,
      initialDialog,
      questionDialog,
      coinText,
      coinsContainer
    }
    // this.questionController.showDialog()
  }

  createInitialDialog() {
    const group = new PIXI.Container()

    const dialog = new PIXI.Sprite(this.resources.dialog1.texture)
    dialog.scale.set(22, 22)
    dialog.position.set(268, 412)

    group.addChild(dialog)


    const character = new PIXI.AnimatedSprite(this.resources.characterTextures)
    character.loop = false
    character.play()
    character.animationSpeed = 0.2
    character.scale.set(1.7, 1.7)
    character.position.set(-225, 225)

    character.onComplete = () => {
      setTimeout(() => {
        character.gotoAndPlay(0)
      }, 700)
    }

    group.addChild(character)

    const style = new PIXI.TextStyle({
      wordWrap: true,
      wordWrapWidth: 850,
      fontFamily: 'Helvetica',
      fill: 0xffffff
    })

    const text = new PIXI.Text('', style)
    text.name = 'Text'
    text.position.set(341, 480)

    group.addChild(text)

    const btn1 = this.utils.loadTextureFromSpritesheet(this.resources.buttonsSmall.texture, 0, 0, 325, 96)
    const btn2 = this.utils.loadTextureFromSpritesheet(this.resources.buttonsSmall.texture, 0, 96, 325, 96)


    const btnGroup = this.utils.createSimpleButton(
      [btn1, btn2],
      new PIXI.Text('Continuar', new PIXI.TextStyle({
        fill: 0xffffff,
        fontFamily: 'Helvetica',
        fontSize: 40,
      })),
      () => this.handleStartDialogClick(),
      938, 606
    )

    btnGroup.alpha = 0
    btnGroup.visible = false
    btnGroup.name = 'Button'

    group.addChild(btnGroup)

    group.y += 500

    return group
  }

  createQuestionDialog() {
    const dialog = new PIXI.Container()
    dialog.name = 'Dialog'

    const bg = new PIXI.Sprite(this.resources.popup.texture)
    bg.name = 'Background'
    dialog.addChild(bg)

    const title = new PIXI.Text('QUESTÃO 1', new PIXI.TextStyle({
      fontWeight: 'bold',
      fontSize: 40,
      fontFamily: 'Helvetica',
      fill: 0xffffff
    }))

    title.anchor.set(0.5, 0, 5)

    title.position.set(bg.width / 2, 37)

    dialog.addChild(title)

    const mask = new PIXI.Sprite(this.resources.popupMask.texture)
    const content = new PIXI.Container()
    content.name = 'Content'
    content.mask = mask
    content.x += this.constants.dialogContentLeftMargin
    content.y += this.constants.dialogContentTopMargin

    dialog.addChild(mask)

    dialog.updateMask = () => {
      mask.position.set(0, 0)
    }

    const questionPage = this.createQuestionPage()

    content.addChild(questionPage)


    const feedbackPages = [
      this.createSuccessPage(),
      this.createErrorPage()
    ]

    for (const page of feedbackPages) {
      page.visible = false
      page.x += (this.constants.dialogContentLeftMargin * 0) + this.constants.dialogContentWidth

      content.addChild(page)
    }

    dialog.addChild(content)

    const dc = this.dialogController = {
      currPage: null,
      canMove: true,
      nextPage: (p) => {
        console.log(dc)
        if (!dc.canMove) return

        dc.canMove = false
        const page = content.getChildByName(p)
        dc.currPage = page
        page.visible = true
        anime({
          targets: content,
          x: '-=' + this.constants.dialogContentWidth,
          easing: 'easeInOutExpo',
          duration: 600,
          complete: () => dc.canMove = true
        })
      },
      prevPage: () => {
        if (!dc.canMove) return

        dc.canMove = false
        anime({
          targets: content,
          x: '+=' + this.constants.dialogContentWidth,
          easing: 'easeInOutExpo',
          duration: 600,
          complete: () => {
            dc.currPage.visible = false
            dc.canMove = true
          }
        })
      }
    }

    this.questionController = {
      setQuestion: async (qi) => {
        const question = questions[qi]
        title.text = `QUESTÃO ${qi + 1}`
        questionPage.getChildByName('QuestionText').text = question.text
        feedbackPages[0].getChildByName('SuccessText').text = question.options.find(o => o.correct).msg
        feedbackPages[1].getChildByName('ErrorText').text = question.options.find(o => !o.correct).msg

        for (let i = 0; i < question.options.length; i++) {
          const option = question.options[i]
          const btn = questionPage.getChildByName('Button' + i)
          btn.correct = !!option.correct

          const el = document.createElement('div')
          // el.style.color = 'black'
          el.innerHTML = `<span>${option.text}</span>`
          document.getElementById('html2canvasContainer').appendChild(el)

          html2canvas(el, {
            backgroundColor: null
          })
            .then((cv) => {

              const ctx = btn._canvas.getContext('2d')
              ctx.fillStyle = 'rgba(0, 0, 0, 0)'
              ctx.clearRect(0, 0, btn._canvas.width, btn._canvas.height)
              ctx.drawImage(cv, 0, 0)

              btn._baseTexture.update()

              el.remove()
            })

        }
      },
      showDialog: () => {
        const tl = anime.timeline()
        tl.add({
          targets: this.items.curtain,
          alpha: 0.7,
          duration: 300,
          easing: 'linear'
        })

        tl.add({
          targets: this.items.questionDialog,
          y: '-=800',
          easing: 'easeOutExpo',
          duration: 600
        })
      },
      closeDialog: (cb = () => { }) => {
        const tl = anime.timeline()
        tl.add({
          targets: this.items.curtain,
          alpha: 0,
          duration: 300,
          easing: 'linear'
        }, 20)

        tl.add({
          targets: this.items.questionDialog,
          y: '+=800',
          easing: 'easeOutExpo',
          duration: 600,
          complete: cb
        })
      }
    }

    return dialog
  }

  createQuestionPage() {
    const page = new PIXI.Container()
    page.name = 'QuestionPage'

    const question = new PIXI.Text(questions[this.question].text, new PIXI.TextStyle({
      fontFamily: 'Helvetica',
      fontSize: 28,
      wordWrap: true,
      align: 'center',
      wordWrapWidth: this.constants.dialogContentWidth - (this.constants.dialogContentMargin * 2)
    }))
    question.name = 'QuestionText'

    question.anchor.set(0.5, 0)

    question.x += this.constants.dialogContentWidth / 2
    question.y += this.constants.dialogContentMargin
    page.addChild(question)

    const btnT1 = this.utils.loadTextureFromSpritesheet(this.resources.buttons.texture, 0, 0, 664, 96)
    const btnT2 = this.utils.loadTextureFromSpritesheet(this.resources.buttons.texture, 0, 96, 664, 96)

    const btnsy = 220

    const cb = b => {
      if (b.correct) {
        zzfx(...[, , 539, 0, .04, .29, 1, 1.92, , , 567, .02, .02, , , , .04])
        this.dialogController.nextPage('SuccessPage')
      } else {
        this.clickSound()
        this.dialogController.nextPage('ErrorPage')
      }
    }

    const createCanvas = () => {
      const c = document.createElement('canvas')
      c.width = btnT1.width
      c.height = btnT1.height
      return c
    }

    const btns = [
      this.utils.createSimpleButton(
        [btnT1, btnT2],
        new PIXI.Sprite(PIXI.Texture.from(createCanvas())),
        cb,
        this.constants.dialogContentWidth / 2,
        btnsy
      ),
      this.utils.createSimpleButton(
        [btnT1, btnT2],
        new PIXI.Sprite(PIXI.Texture.from(createCanvas())),
        cb,
        this.constants.dialogContentWidth / 2,
        btnsy + 97
      )
    ]

    for (let i = 0; i < btns.length; i++) {
      const btn = btns[i]
      btn.name = 'Button' + i
      btn.scale.set(0.95, 0.95)
      btn.correct = false
      btn.x -= btn.width / 2

      btn._baseTexture = btn.textOverlay.texture.baseTexture
      btn._canvas = btn.textOverlay.texture.baseTexture.resource.source

      page.addChild(btn)
    }

    return page
  }

  createSuccessPage() {
    const page = new PIXI.Container()
    page.name = 'SuccessPage'

    const success = new PIXI.Text('Parabéns!', new PIXI.TextStyle({
      fontFamily: 'Helvetica',
      fontSize: 60,
      align: 'center',
    }))

    const msg = new PIXI.Text(questions[this.question].text, new PIXI.TextStyle({
      fontFamily: 'Helvetica',
      fontSize: 28,
      wordWrap: true,
      align: 'center',
      wordWrapWidth: this.constants.dialogContentWidth - (this.constants.dialogContentMargin * 2)
    }))
    msg.name = 'SuccessText'

    msg.anchor.set(0.5, 0)

    msg.x += this.constants.dialogContentWidth / 2
    msg.y = 100
    page.addChild(msg)

    success.anchor.set(0.5, 0.5)

    success.x += this.constants.dialogContentWidth / 2
    success.y += this.constants.dialogContentMargin + 30
    page.addChild(success)


    const btnT1 = this.utils.loadTextureFromSpritesheet(this.resources.buttons.texture, 0, 0, 664, 96)
    const btnT2 = this.utils.loadTextureFromSpritesheet(this.resources.buttons.texture, 0, 96, 664, 96)

    let canClick = true

    const btn = this.utils.createSimpleButton(
      [btnT1, btnT2],
      new PIXI.Text('Continuar', new PIXI.TextStyle({
        fill: 0xffffff,
        fontFamily: 'Helvetica',
        fontSize: 30,
        fontWeight: 400,
      })),
      () => {
        if (!canClick) return
        this.coins++
        zzfx(...[,,1675,,.06,.24,1,1.82,,,837,.06])
        canClick = false
        this.questionController.closeDialog(() => {
          canClick = true
          this.dialogController.prevPage()
          const hasNext = !!questions[this.question + 1]
          this.video.currentTime = questions[this.question].times[1]
          this.video.play()
          if (hasNext) {
            this.question++
            this.questionController.setQuestion(this.question)
          }
        })
      },
      this.constants.dialogContentWidth / 2,
      220 + 97
    )

    btn.scale.set(0.95, 0.95)
    btn.answear = false
    btn.x -= btn.width / 2

    page.addChild(btn)

    return page
  }


  createErrorPage() {
    const page = new PIXI.Container()
    page.name = 'ErrorPage'

    const error = new PIXI.Text('Tente novamente!', new PIXI.TextStyle({
      fontFamily: 'Helvetica',
      fontSize: 60,
      align: 'center',
    }))

    const msg = new PIXI.Text(questions[this.question].text, new PIXI.TextStyle({
      fontFamily: 'Helvetica',
      fontSize: 28,
      wordWrap: true,
      align: 'center',
      wordWrapWidth: this.constants.dialogContentWidth - (this.constants.dialogContentMargin * 2)
    }))
    msg.name = 'ErrorText'

    msg.anchor.set(0.5, 0)

    msg.x += this.constants.dialogContentWidth / 2
    msg.y = 100
    page.addChild(msg)



    error.anchor.set(0.5, 0.5)

    error.x += this.constants.dialogContentWidth / 2
    error.y += this.constants.dialogContentMargin + 30
    page.addChild(error)


    const btnT1 = this.utils.loadTextureFromSpritesheet(this.resources.buttons.texture, 0, 0, 664, 96)
    const btnT2 = this.utils.loadTextureFromSpritesheet(this.resources.buttons.texture, 0, 96, 664, 96)

    const btn = this.utils.createSimpleButton(
      [btnT1, btnT2],
      new PIXI.Text('Tentar novamente', new PIXI.TextStyle({
        fill: 0xffffff,
        fontFamily: 'Helvetica',
        fontSize: 30,
        fontWeight: 400,
      })),
      () => {
        this.clickSound()
        this.dialogController.prevPage()
      },
      this.constants.dialogContentWidth / 2,
      220 + 97
    )

    btn.scale.set(0.95, 0.95)
    btn.x -= btn.width / 2

    page.addChild(btn)

    return page
  }

  clickSound() {
    zzfx(...[1.01, , 418, , , .17, , .39, , , , , , , -2, , , .75, .02, .19])
  }

  handleStartDialogClick() {
    this.clickSound()
    this.video.currentTime = 15.1
    const btnItem = this.items.initialDialog.getChildByName('Button')
    btnItem.visible = false

    const tl = anime.timeline()

    tl.add({
      targets: this.items.initialDialog,
      y: '+=500',
      easing: 'easeInExpo',
      duration: 600
    })

    tl.add({
      targets: this.items.curtain,
      alpha: 0,
      duration: 400,
      easing: 'linear',
      complete: () => this.video.play()
    }, 200)
  }

  setVideoStops() {
    this.stops = new Map()
    const addVideoStop = (time, callback) => {
      this.stops.set(time, {
        time, callback, triggered: false
      })
    }

    addVideoStop(11.5, () => {
      this.video.pause()
      this.questionController.setQuestion(0)

      const tl = anime.timeline()

      tl.add({
        targets: this.items.curtain,
        alpha: 0.7,
        duration: 300,
        easing: 'linear'
      })

      tl.add({
        targets: this.items.initialDialog,
        y: '-=500',
        easing: 'easeOutExpo',
        duration: 600
      })

      const words = (this.level.helpText + ' ').split(' ')

      const textItem = this.items.initialDialog.getChildByName('Text')
      const btnItem = this.items.initialDialog.getChildByName('Button')

      for (let i = 0; i < words.length; i++) {
        const word = words[i]
        console.log(word)
        tl.add({
          targets: this.items.initialDialog,
          duration: 1,
          begin: () => {
            textItem.text += word + ' '
            zzfx(...[,,224,.02,.02,.08,1,1.7,-13.9,,,,,,6.7])
          }
        }, 900 + (i * this.constants.speechWordDelay))
      }

      tl.add({
        targets: btnItem,
        alpha: 1,
        easing: 'linear',
        begin: () => {
          btnItem.visible = true
        }
      }, 4000)
    })

    for (let i = 0; i < questions.length; i++) {
      const question = questions[i]

      addVideoStop(question.times[0], () => {
        this.video.pause()
        this.questionController.showDialog()


        // LAST STEP
        // if (hasNext) {
        //   this.questionController.set
        // }
      })

    }

    addVideoStop(68, () => {
      this.video.pause()

      const tl = anime.timeline()
      
      tl.add({
        targets: this.items.curtain,
        alpha: 0.7,
        duration: 300,
        easing: 'linear'
      })

      tl.add({
        targets: this.items.initialDialog,
        y: '-=500',
        easing: 'easeOutExpo',
        duration: 600
      })

      const words = (this.level.endText + ' ').split(' ')

      const textItem = this.items.initialDialog.getChildByName('Text')
      textItem.text = ''

      for (let i = 0; i < words.length; i++) {
        const word = words[i]
        console.log(word + ` - ${i}`)
        tl.add({
          targets: this.items.initialDialog,
          duration: 1,
          begin: () => {
            textItem.text += word + ' '
            zzfx(...[,,224,.02,.02,.08,1,1.7,-13.9,,,,,,6.7])
          }
        }, 900 + (i * this.constants.speechWordDelay) + (i > 6 ? 700 : 0))
      }
    })

  }

  start() {
    this.startPlayer()
    this.started = true
    this.items.thumbnail.visible = false
    const { video } = this
    video.play()

    video.addEventListener('timeupdate', () => this.handleVideoStop())
  }

  handleVideoStop() {
    const currTime = this.video.currentTime
    for (const [time, stop] of this.stops) {
      if (currTime >= time && !stop.triggered) {
        stop.triggered = true
        stop.callback()
      }
    }
  }

  tick() {
    return
    if (this.video && this.started && this.stops) {
      this.videoSkip++

      if (this.videoSkip > 5) {
        this.videoSkip = 0
        this.handleVideoStop()
      }
    }
  }

  get coins() {
    return this._coins
  }

  set coins(c) {
    this._coins = c
    this.items.coinText.text = c
    anime({
      targets: this.items.coinsContainer.scale,
      x: 1.3,
      y: 1.3,
      easing: 'easeInBack',
      duration: 300,
      complete: () => {
        anime({
          targets: this.items.coinsContainer.scale,
          x: 1,
          y: 1,
          easing: 'easeOutBack',
          duration: 300,
        })
      }
    })
  }
}